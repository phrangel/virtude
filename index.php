﻿<?php

session_start();

if(!isset($_SESSION['user_session']))
{
	header("Location: login.php");
}

?>

<!DOCTYPE html>
<html lang="pt-BR" >
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="format-detection" content="telephone=no">
	<title>Você &#8211; Virtude</title>
<script type="text/javascript">var ajaxurl = "http://zoi.com.br/virtude/wp-admin/admin-ajax.php"</script>


<style id="critical-path-css" type='text/css'>
	body,html{width:100%;height:100%;margin:0;padding:0;}
	.page-preloader{top:0;
					left:0;
					z-index:999;
					position:fixed;
					height:90%;
					width:90%;
					text-align:center}
	.preloader-logo,
	.preloader-preview-area{top:50%;
							max-height:calc(50% - 20px);
							opacity:1}
	.preloader-preview-area{-webkit-animation-delay:-.2s;animation-delay:-.2s;-webkit-transform:translateY(100%);-ms-transform:translateY(100%);transform:translateY(100%);margin-top:10px;width:100%;text-align:center;position:absolute}.preloader-logo{max-width:90%;-webkit-transform:translateY(-100%);-ms-transform:translateY(-100%);transform:translateY(-100%);margin:-10px auto 0;position:relative}.ball-pulse>div,.ball-scale>div,.line-scale>div{margin:2px;display:inline-block}.ball-pulse>div{width:15px;height:15px;border-radius:100%;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation:ball-pulse .75s infinite cubic-bezier(.2,.68,.18,1.08);animation:ball-pulse .75s infinite cubic-bezier(.2,.68,.18,1.08)}.ball-pulse>div:nth-child(1){-webkit-animation-delay:-.36s;animation-delay:-.36s}.ball-pulse>div:nth-child(2){-webkit-animation-delay:-.24s;animation-delay:-.24s}.ball-pulse>div:nth-child(3){-webkit-animation-delay:-.12s;animation-delay:-.12s}@-webkit-keyframes ball-pulse{0%,80%{-webkit-transform:scale(1);transform:scale(1);opacity:1}45%{-webkit-transform:scale(.1);transform:scale(.1);opacity:.7}}@keyframes ball-pulse{0%,80%{-webkit-transform:scale(1);transform:scale(1);opacity:1}45%{-webkit-transform:scale(.1);transform:scale(.1);opacity:.7}}.ball-clip-rotate-pulse{position:relative;-webkit-transform:translateY(-15px) translateX(-10px);-ms-transform:translateY(-15px) translateX(-10px);transform:translateY(-15px) translateX(-10px);display:inline-block}.ball-clip-rotate-pulse>div{-webkit-animation-fill-mode:both;animation-fill-mode:both;position:absolute;top:0;left:0;border-radius:100%}.ball-clip-rotate-pulse>div:first-child{height:36px;width:36px;top:7px;left:-7px;-webkit-animation:ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09,.57,.49,.9) infinite;animation:ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09,.57,.49,.9) infinite}.ball-clip-rotate-pulse>div:last-child{position:absolute;width:50px;height:50px;left:-16px;top:-2px;background:0 0;border:2px solid;-webkit-animation:ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09,.57,.49,.9) infinite;animation:ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09,.57,.49,.9) infinite;-webkit-animation-duration:1s;animation-duration:1s}@-webkit-keyframes ball-clip-rotate-pulse-rotate{0%{-webkit-transform:rotate(0) scale(1);transform:rotate(0) scale(1)}50%{-webkit-transform:rotate(180deg) scale(.6);transform:rotate(180deg) scale(.6)}100%{-webkit-transform:rotate(360deg) scale(1);transform:rotate(360deg) scale(1)}}@keyframes ball-clip-rotate-pulse-rotate{0%{-webkit-transform:rotate(0) scale(1);transform:rotate(0) scale(1)}50%{-webkit-transform:rotate(180deg) scale(.6);transform:rotate(180deg) scale(.6)}100%{-webkit-transform:rotate(360deg) scale(1);transform:rotate(360deg) scale(1)}}@-webkit-keyframes ball-clip-rotate-pulse-scale{30%{-webkit-transform:scale(.3);transform:scale(.3)}100%{-webkit-transform:scale(1);transform:scale(1)}}@keyframes ball-clip-rotate-pulse-scale{30%{-webkit-transform:scale(.3);transform:scale(.3)}100%{-webkit-transform:scale(1);transform:scale(1)}}@-webkit-keyframes square-spin{25%{-webkit-transform:perspective(100px) rotateX(180deg) rotateY(0);transform:perspective(100px) rotateX(180deg) rotateY(0)}50%{-webkit-transform:perspective(100px) rotateX(180deg) rotateY(180deg);transform:perspective(100px) rotateX(180deg) rotateY(180deg)}75%{-webkit-transform:perspective(100px) rotateX(0) rotateY(180deg);transform:perspective(100px) rotateX(0) rotateY(180deg)}100%{-webkit-transform:perspective(100px) rotateX(0) rotateY(0);transform:perspective(100px) rotateX(0) rotateY(0)}}@keyframes square-spin{25%{-webkit-transform:perspective(100px) rotateX(180deg) rotateY(0);transform:perspective(100px) rotateX(180deg) rotateY(0)}50%{-webkit-transform:perspective(100px) rotateX(180deg) rotateY(180deg);transform:perspective(100px) rotateX(180deg) rotateY(180deg)}75%{-webkit-transform:perspective(100px) rotateX(0) rotateY(180deg);transform:perspective(100px) rotateX(0) rotateY(180deg)}100%{-webkit-transform:perspective(100px) rotateX(0) rotateY(0);transform:perspective(100px) rotateX(0) rotateY(0)}}.square-spin{display:inline-block}.square-spin>div{-webkit-animation-fill-mode:both;animation-fill-mode:both;width:50px;height:50px;-webkit-animation:square-spin 3s 0s cubic-bezier(.09,.57,.49,.9) infinite;animation:square-spin 3s 0s cubic-bezier(.09,.57,.49,.9) infinite}.cube-transition{position:relative;-webkit-transform:translate(-25px,-25px);-ms-transform:translate(-25px,-25px);transform:translate(-25px,-25px);display:inline-block}.cube-transition>div{-webkit-animation-fill-mode:both;animation-fill-mode:both;width:15px;height:15px;position:absolute;top:-5px;left:-5px;-webkit-animation:cube-transition 1.6s 0s infinite ease-in-out;animation:cube-transition 1.6s 0s infinite ease-in-out}.cube-transition>div:last-child{-webkit-animation-delay:-.8s;animation-delay:-.8s}@-webkit-keyframes cube-transition{25%{-webkit-transform:translateX(50px) scale(.5) rotate(-90deg);transform:translateX(50px) scale(.5) rotate(-90deg)}50%{-webkit-transform:translate(50px,50px) rotate(-180deg);transform:translate(50px,50px) rotate(-180deg)}75%{-webkit-transform:translateY(50px) scale(.5) rotate(-270deg);transform:translateY(50px) scale(.5) rotate(-270deg)}100%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg)}}@keyframes cube-transition{25%{-webkit-transform:translateX(50px) scale(.5) rotate(-90deg);transform:translateX(50px) scale(.5) rotate(-90deg)}50%{-webkit-transform:translate(50px,50px) rotate(-180deg);transform:translate(50px,50px) rotate(-180deg)}75%{-webkit-transform:translateY(50px) scale(.5) rotate(-270deg);transform:translateY(50px) scale(.5) rotate(-270deg)}100%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg)}}.ball-scale>div{border-radius:100%;-webkit-animation-fill-mode:both;animation-fill-mode:both;height:60px;width:60px;-webkit-animation:ball-scale 1s 0s ease-in-out infinite;animation:ball-scale 1s 0s ease-in-out infinite}.ball-scale-multiple>div,.line-scale>div{-webkit-animation-fill-mode:both;height:50px}@-webkit-keyframes ball-scale{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}@keyframes ball-scale{0%{-webkit-transform:scale(0);transform:scale(0)}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}.line-scale>div{animation-fill-mode:both;width:5px;border-radius:2px}.line-scale>div:nth-child(1){-webkit-animation:line-scale 1s -.5s infinite cubic-bezier(.2,.68,.18,1.08);animation:line-scale 1s -.5s infinite cubic-bezier(.2,.68,.18,1.08)}.line-scale>div:nth-child(2){-webkit-animation:line-scale 1s -.4s infinite cubic-bezier(.2,.68,.18,1.08);animation:line-scale 1s -.4s infinite cubic-bezier(.2,.68,.18,1.08)}.line-scale>div:nth-child(3){-webkit-animation:line-scale 1s -.3s infinite cubic-bezier(.2,.68,.18,1.08);animation:line-scale 1s -.3s infinite cubic-bezier(.2,.68,.18,1.08)}.line-scale>div:nth-child(4){-webkit-animation:line-scale 1s -.2s infinite cubic-bezier(.2,.68,.18,1.08);animation:line-scale 1s -.2s infinite cubic-bezier(.2,.68,.18,1.08)}.line-scale>div:nth-child(5){-webkit-animation:line-scale 1s -.1s infinite cubic-bezier(.2,.68,.18,1.08);animation:line-scale 1s -.1s infinite cubic-bezier(.2,.68,.18,1.08)}@-webkit-keyframes line-scale{0%,100%{-webkit-transform:scaley(1);transform:scaley(1)}50%{-webkit-transform:scaley(.4);transform:scaley(.4)}}@keyframes line-scale{0%,100%{-webkit-transform:scaley(1);transform:scaley(1)}50%{-webkit-transform:scaley(.4);transform:scaley(.4)}}.ball-scale-multiple{position:relative;-webkit-transform:translateY(30px);-ms-transform:translateY(30px);transform:translateY(30px);display:inline-block}.ball-scale-multiple>div{border-radius:100%;animation-fill-mode:both;margin:0;position:absolute;left:-30px;top:0;opacity:0;width:50px;-webkit-animation:ball-scale-multiple 1s 0s linear infinite;animation:ball-scale-multiple 1s 0s linear infinite}.ball-scale-multiple>div:nth-child(2),.ball-scale-multiple>div:nth-child(3){-webkit-animation-delay:-.2s;animation-delay:-.2s}@-webkit-keyframes ball-scale-multiple{0%{-webkit-transform:scale(0);transform:scale(0);opacity:0}5%{opacity:1}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}@keyframes ball-scale-multiple{0%{-webkit-transform:scale(0);transform:scale(0);opacity:0}5%{opacity:1}100%{-webkit-transform:scale(1);transform:scale(1);opacity:0}}.ball-pulse-sync{display:inline-block}.ball-pulse-sync>div{width:15px;height:15px;border-radius:100%;margin:2px;-webkit-animation-fill-mode:both;animation-fill-mode:both;display:inline-block}.ball-pulse-sync>div:nth-child(1){-webkit-animation:ball-pulse-sync .6s -.21s infinite ease-in-out;animation:ball-pulse-sync .6s -.21s infinite ease-in-out}.ball-pulse-sync>div:nth-child(2){-webkit-animation:ball-pulse-sync .6s -.14s infinite ease-in-out;animation:ball-pulse-sync .6s -.14s infinite ease-in-out}.ball-pulse-sync>div:nth-child(3){-webkit-animation:ball-pulse-sync .6s -70ms infinite ease-in-out;animation:ball-pulse-sync .6s -70ms infinite ease-in-out}@-webkit-keyframes ball-pulse-sync{33%{-webkit-transform:translateY(10px);transform:translateY(10px)}66%{-webkit-transform:translateY(-10px);transform:translateY(-10px)}100%{-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes ball-pulse-sync{33%{-webkit-transform:translateY(10px);transform:translateY(10px)}66%{-webkit-transform:translateY(-10px);transform:translateY(-10px)}100%{-webkit-transform:translateY(0);transform:translateY(0)}}.transparent-circle{display:inline-block;border-top:.5em solid rgba(255,255,255,.2);border-right:.5em solid rgba(255,255,255,.2);border-bottom:.5em solid rgba(255,255,255,.2);border-left:.5em solid #fff;-webkit-transform:translateZ(0);transform:translateZ(0);-webkit-animation:transparent-circle 1.1s infinite linear;animation:transparent-circle 1.1s infinite linear;width:50px;height:50px;border-radius:50%}.transparent-circle:after{border-radius:50%;width:10em;height:10em}@-webkit-keyframes transparent-circle{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes transparent-circle{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.ball-spin-fade-loader{position:relative;top:-10px;left:-10px;display:inline-block}.ball-spin-fade-loader>div{width:15px;height:15px;border-radius:100%;margin:2px;-webkit-animation-fill-mode:both;animation-fill-mode:both;position:absolute;-webkit-animation:ball-spin-fade-loader 1s infinite linear;animation:ball-spin-fade-loader 1s infinite linear}.ball-spin-fade-loader>div:nth-child(1){top:25px;left:0;animation-delay:-.84s;-webkit-animation-delay:-.84s}.ball-spin-fade-loader>div:nth-child(2){top:17.05px;left:17.05px;animation-delay:-.72s;-webkit-animation-delay:-.72s}.ball-spin-fade-loader>div:nth-child(3){top:0;left:25px;animation-delay:-.6s;-webkit-animation-delay:-.6s}.ball-spin-fade-loader>div:nth-child(4){top:-17.05px;left:17.05px;animation-delay:-.48s;-webkit-animation-delay:-.48s}.ball-spin-fade-loader>div:nth-child(5){top:-25px;left:0;animation-delay:-.36s;-webkit-animation-delay:-.36s}.ball-spin-fade-loader>div:nth-child(6){top:-17.05px;left:-17.05px;animation-delay:-.24s;-webkit-animation-delay:-.24s}.ball-spin-fade-loader>div:nth-child(7){top:0;left:-25px;animation-delay:-.12s;-webkit-animation-delay:-.12s}.ball-spin-fade-loader>div:nth-child(8){top:17.05px;left:-17.05px;animation-delay:0s;-webkit-animation-delay:0s}@-webkit-keyframes ball-spin-fade-loader{50%{opacity:.3;-webkit-transform:scale(.4);transform:scale(.4)}100%{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}@keyframes ball-spin-fade-loader{50%{opacity:.3;-webkit-transform:scale(.4);transform:scale(.4)}100%{opacity:1;-webkit-transform:scale(1);transform:scale(1)}}
	.callMenu {
		
		position: absolute;    /* para poder mover o boão para fora da caixa do menu */
		left: -54px;           /* coloca o botão de fora tornando-o clicável*/
		background-color: transparent;
		cursor: pointer;
	
	}
	.labelPreto{
		color: black;  !important
	}
	#fixed{
	position:fixed;
	width:400px;
	height:100px;
	left:300px;
	top: 30%;
	background:transparent;
	}
	#msgSucesso{
	width:100%;
	height:100%;
	}

	.navBar {
		width: 600px;
		height: 100%;
		right: -600px;  /* Menu inicia escondido [valor igual ao width] */
		top: 0;
		background-color: white;
		color: #fff;
		position: fixed; /* Todo o menu fica fixo, assim ele acompanha a página ao fazermos scroll */
		/* Faz a animação do menu deslizar em vez de aparecer logo aberto */
		-webkit-transition: all 0.5s ease-in-out;
		-moz-transition: all 0.5s ease-in-out;
		-o-transition: all 0.5s ease-in-out;
		transition: all 0.5s ease-in-out;
	
		-webkit-box-shadow: 0px 0px 10px #999 inset;
		-moz-box-shadow: 0px 0px 10px #999 inset;
			box-shadow: 0px 0px 10px #999 inset;
	}
	.slideMenu {
		right: 0; /* Coloca o menu na posiçáo desejada de maneira a ficar visível */
	}
	.navBar ul {
		list-style: none;
		margin: 0;
		
	}
	#exTab1 .tab-content {
	color : white;
	background-color: white;
	padding : 5px 15px;
	width: 580px;
	}
	
	#exTab2 h3 {
	color : white;
	background-color: #428bca;
	padding : 5px 15px;
	}
	
	/* remove border radius for the tab */
	
	#exTab1 .nav-pills > li > a {
	border-radius: 0;
	}
	
	/* change border radius for the tab , apply corners on top*/
	
	#exTab3 .nav-pills > li > a {
	border-radius: 4px 4px 0 0 ;
	}
	
	#exTab3 .tab-content {
	color : white;
	background-color: #428bca;
	padding : 5px 15px;
	}
	.glyphicon-refresh-animate {
		-animation: spin .7s infinite linear;
		-webkit-animation: spin2 .7s infinite linear;
	}

	@-webkit-keyframes spin2 {
		from { -webkit-transform: rotate(0deg);}
		to { -webkit-transform: rotate(360deg);}
	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg);}
		to { transform: scale(1) rotate(360deg);}
	}

@media screen and (min-width: 768px) {
    .modal-dialog {
        left: 28%;
        right: auto;
        width: 624px;
    }
}

.modal-dialog{
   position: absolute;
   left: 28%;
   margin-left: -312px;
   height: 500px;
   top: 0%;
   margin-top: -250px;
} 
</style>

<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Feed para Virtude &raquo;" href="http://zoi.com.br/virtude/feed/" />
<link rel="alternate" type="application/rss+xml" title="Feed de comentários para Virtude &raquo;" href="http://zoi.com.br/virtude/comments/feed/" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap.min.css" rel="stylesheet">
<link rel="shortcut icon" href="http://zoi.com.br/virtude/wp-content/uploads/2017/03/favicon.png"  />
<script type="text/javascript">
	window.abb = {};
	php = {};
	window.PHP = {};
	PHP.ajax = "http://zoi.com.br/virtude/wp-admin/admin-ajax.php";PHP.wp_p_id = "5";var mk_header_parallax, mk_banner_parallax, mk_page_parallax, mk_footer_parallax, mk_body_parallax;
	var mk_images_dir = "http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/images",
	mk_theme_js_path = "http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/js",
	mk_theme_dir = "http://zoi.com.br/virtude/wp-content/themes/jupiter",
	mk_captcha_placeholder = "Enter Captcha",
	mk_captcha_invalid_txt = "Invalid. Try again.",
	mk_captcha_correct_txt = "Captcha correct.",
	mk_responsive_nav_width = 1140,
	mk_vertical_header_back = "Back",
	mk_vertical_header_anim = "1",
	mk_check_rtl = true,
	mk_grid_width = 1140,
	mk_ajax_search_option = "fullscreen_search",
	mk_preloader_bg_color = "#ffffff",
	mk_accent_color = "#ffa32b",
	mk_go_to_top =  "true",
	mk_smooth_scroll =  "true",
	mk_preloader_bar_color = "#ffa32b",
	mk_preloader_logo = "";
	var mk_header_parallax = false,
	mk_banner_parallax = false,
	mk_page_parallax = false,
	mk_footer_parallax = false,
	mk_body_parallax = false,
	mk_no_more_posts = "No More Posts";
</script>

<script type="text/javascript">
	window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/zoi.com.br\/virtude\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.3"}};
	!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>

<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 .07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
</style>
<link rel='stylesheet' id='theme-styles-css'  href='http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/min/core-styles.css?ver=5.2' type='text/css' media='all' />
<link rel='stylesheet' id='google-font-api-special-1-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%2C100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.3' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='http://zoi.com.br/virtude/wp-content/plugins/js_composer_theme/assets/css/js_composer.min.css?ver=4.12.1' type='text/css' media='all' />
<link rel='stylesheet' id='global-assets-css-css'  href='http://zoi.com.br/virtude/wp-content/uploads/mk_assets/components-production.min.css?ver=1490814244' type='text/css' media='all' />
<link rel='stylesheet' id='theme-options-css'  href='http://zoi.com.br/virtude/wp-content/uploads/mk_assets/theme-options-production.css?ver=1490814244' type='text/css' media='all' />
<link rel='stylesheet' id='mk-style-css'  href='http://zoi.com.br/virtude/wp-content/themes/jupiter/style.css?ver=4.7.3' type='text/css' media='all' />
<link rel='stylesheet' id='theme-dynamic-styles-css'  href='http://zoi.com.br/virtude/wp-content/themes/jupiter/custom.css?ver=4.7.3' type='text/css' media='all' />

<style id='theme-dynamic-styles-inline-css' type='text/css'>
	body { background-color:#fff; } .mk-header { background-color:#f7f7f7;background-size:cover;-webkit-background-size:cover;-moz-background-size:cover; } .mk-header-bg { background-color:#fff; } .mk-classic-nav-bg { background-color:#fff; } #theme-page { background-color:#fff; } #mk-footer { background-color:#f2f2f2; } #mk-boxed-layout { -webkit-box-shadow:0 0 0px rgba(0, 0, 0, 0); -moz-box-shadow:0 0 0px rgba(0, 0, 0, 0); box-shadow:0 0 0px rgba(0, 0, 0, 0); } .mk-news-tab .mk-tabs-tabs .is-active a, .mk-fancy-title.pattern-style span, .mk-fancy-title.pattern-style.color-gradient span:after, .page-bg-color { background-color:#fff; } .page-title { font-size:20px; color:#4d4d4d; text-transform:uppercase; font-weight:400; letter-spacing:2px; } .page-subtitle { font-size:14px; line-height:100%; color:#a3a3a3; font-size:14px; text-transform:none; } @font-face { font-family:'star'; src:url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot'); src:url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot?#iefix') format('embedded-opentype'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.woff') format('woff'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.ttf') format('truetype'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.svg#star') format('svg'); font-weight:normal; font-style:normal; } @font-face { font-family:'WooCommerce'; src:url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot'); src:url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot?#iefix') format('embedded-opentype'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.woff') format('woff'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.ttf') format('truetype'), url('http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.svg#WooCommerce') format('svg'); font-weight:normal; font-style:normal; } #text-block-3 { margin-bottom:0px; text-align:left; } .full-width-2 { min-height:800px; margin-bottom:0px; background-color:#ffa32b; } .full-width-2 .page-section-content { padding:10px 0 10px; } #background-layer--2 { background-position:center top; background-repeat:no-repeat; position:absolute;; ; } .full-width-2 .mk-fancy-title.pattern-style span, .full-width-2 .mk-blog-view-all { background-color:#ffa32b !important; } #text-block-5 { margin-bottom:0px; text-align:center; } #text-block-6 { margin-bottom:0px; text-align:center; } .full-width-4 { min-height:400px; margin-bottom:0px; background-color:#f9f9f9; } .full-width-4 .page-section-content { padding:120px 0 120px; } #background-layer--4 { background-position:center top; background-repeat:no-repeat; position:fixed;; ; } .full-width-4 .mk-fancy-title.pattern-style span, .full-width-4 .mk-blog-view-all { background-color:#f9f9f9 !important; } #text-block-8 { margin-bottom:50px; text-align:center; } #text-block-9 { margin-bottom:0px; text-align:center; } #text-block-10 { margin-bottom:0px; text-align:center; } #text-block-11 { margin-bottom:0px; text-align:center; } #mk-button-12 { margin-bottom:15px; margin-top:35px; margin-right:0px; } #mk-button-12 .mk-button { background-color:#ffa32b; } #mk-button-12 .mk-button:hover { } #mk-button-12 .mk-button:hover .mk-svg-icon { } .full-width-7 { min-height:px; margin-bottom:0px; background-color:#595959; } .full-width-7 .page-section-content { padding:120px 0 120px; } #background-layer--7 { background-position:center center; background-repeat:no-repeat; position:absolute;; ; } .full-width-7 .mk-fancy-title.pattern-style span, .full-width-7 .mk-blog-view-all { background-color:#595959 !important; } #text-block-14 { margin-bottom:0px; text-align:center; } .s_contact.s_classic .s_txt-input { background:url(http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/images/contact-inputs-bg.png) left top repeat-y #ffffff; } .full-width-13 { min-height:400px; margin-bottom:0px; background-color:#f9f9f9; } .full-width-13 .page-section-content { padding:120px 0 120px; } #background-layer--13 { background-position:center top; background-repeat:no-repeat; position:fixed;; ; } .full-width-13 .mk-fancy-title.pattern-style span, .full-width-13 .mk-blog-view-all { background-color:#f9f9f9 !important; }
</style>

<script type='text/javascript' src='http://zoi.com.br/virtude/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>



<link rel='https://api.w.org/' href='http://zoi.com.br/virtude/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://zoi.com.br/virtude/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://zoi.com.br/virtude/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.3" />
<link rel="canonical" href="http://zoi.com.br/virtude/voce/" />
<link rel='shortlink' href='http://zoi.com.br/virtude/?p=5' />
<link rel="alternate" type="application/json+oembed" href="http://zoi.com.br/virtude/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fzoi.com.br%2Fvirtude%2Fvoce%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://zoi.com.br/virtude/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fzoi.com.br%2Fvirtude%2Fvoce%2F&#038;format=xml" />
<script> var isTest = false; </script>
<style id="js-media-query-css">.mk-event-countdown-ul:media( max-width: 750px ) li{width:90%;display:block;margin:0 auto 15px}.mk-process-steps:media( max-width: 960px ) ul:before{display:none!important}.mk-process-steps:media( max-width: 960px ) li{margin-bottom:30px!important;width:100%!important;text-align:center}</style>		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://zoi.com.br/virtude/wp-content/plugins/js_composer_theme/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://zoi.com.br/virtude/wp-content/plugins/js_composer_theme/assets/css/vc-ie8.min.css" media="screen"><![endif]--><meta name="generator" content="jupiter 5.2" />
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="js/jquery.form.js"></script>


<body class="page-template-default page page-id-5 loading wpb-js-composer js-comp-ver-4.12.1 vc_responsive" itemscope="itemscope" itemtype="https://schema.org/WebPage"  data-adminbar="">

	<div class="mk-body-loader-overlay page-preloader" style="background-color:#ffffff;"> 
		<div class="preloader-preview-area">  
			<div class="ball-pulse">
                <div style="background-color: #7c7c7c"></div>
				<div style="background-color: #7c7c7c"></div>
				<div style="background-color: #7c7c7c"></div>
			</div>  
		</div>
	</div>
	<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
	<div id="top-of-page"></div>
		<div id="mk-boxed-layout">
			<div id="mk-theme-container" >
				<div id="theme-page" class="master-holder  clearfix" role="main" itemprop="mainContentOfPage" >		
					<div class="mk-main-wrapper-holder">
						<div id="mk-page-id-5" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout no-padding ">
							<div class="theme-content no-padding" itemprop="mainContentOfPage">
								<div class="clearboth"></div>
							</div> 		
						</div> 		
					</div> 
					<div class="mk-page-section-wrapper">
						<div id="page-section-2" class="mk-page-section self-hosted  full-width-2 js-el js-master-row     center-y"    data-intro-effect="false">
							<div class="mk-page-section-inner">
								<div class="mk-video-color-mask"></div>
								<div class="mk-section-preloader js-el" data-mk-component="Preloader">
									<div class="mk-section-preloader__icon"></div>
								</div>
								
								<div class="background-layer-holder">
									<div id="background-layer--2" class="background-layer  none-blend-effect js-el" data-mk-component="Parallax" data-parallax-config='{"speed" : 0.2 }'  data-mk-img-set='{"landscape":{"desktop":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/downstairsstudio-2-1.jpg","tablet":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/downstairsstudio-2-1-1024x768.jpg","mobile":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/downstairsstudio-2-1-736x414.jpg"}}' >
									</div>									
								</div>
							</div> 
							
							<div class="page-section-content vc_row-fluid page-section-fullwidth">
								<div class="mk-padding-wrapper">
									<div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
										<div id="text-block-3" class="mk-text-block   ">	
											<h1 style="text-align: center;" id="saudacaoVirtude"><strong>[Saudação],</strong></h1>
											<h1 style="text-align: center;" id="virtude"><strong>[Cliente] !</strong></h1>
											<h3 style="text-align: center;" id="introducaoVirtude">[Mensagem de boas-vindas]</h3>
											<div class="clearboth"></div>
										</div>
									</div>
								</div>
								<div class="clearboth"></div>
							</div>
							
							<div class="mk-skip-to-next" data-skin="light">
								<svg  class="mk-svg-icon" data-name="mk-jupiter-icon-arrow-bottom" data-cacheid="icon-58d12c9633921" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 121.6c0 8-3.2 16-8 22.4l-225.6 240c-6.4 6.4-14.4 9.6-24 9.6-8 0-16-3.2-22.4-9.6l-224-240c-11.2-12.8-11.2-33.6 1.6-44.8 12.8-12.8 32-11.2 44.8 1.6l201.6 214.4 203.2-216c11.2-12.8 32-12.8 44.8 0 4.8 6.4 8 14.4 8 22.4z"/></svg>
							</div>
							
							<div class="clearboth"></div>
						</div>
					</div>
					
					<div class="mk-main-wrapper-holder">
						<div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
							<div class="theme-content no-padding">
								<div class="clearboth"></div>
							</div> 		
						</div>
					</div>
					
					<div class="mk-page-section-wrapper">
						<div id="page-section-4" class="mk-page-section self-hosted  full-width-4 js-el js-master-row     center-y"    data-intro-effect="false">
							<div class="mk-page-section-inner">
								<div class="mk-video-color-mask"></div>
                            </div>
           
							<div class="page-section-content vc_row-fluid page-section-fullwidth">
								<div class="mk-padding-wrapper">
									<div style="" class="vc_col-sm-4 wpb_column column_container  _ height-full">
										<div class="wpb_single_image wpb_content_element vc_align_center">
											<figure class="wpb_wrapper vc_figure">
												<div class="vc_single_image-wrapper   vc_box_border_grey">
													<img id="img-upload" width="250" height="162" src="uploads/LogoAqui.png"  alt="" sizes="(max-width: 250px) 100vw, 250px" />
												</div>
											</figure>
										</div>
									</div>
									
									<div style="" class="vc_col-sm-4 wpb_column column_container  _ height-full">
										<div id="text-block-5" class="mk-text-block  mk-animate-element fade-in   ">
											<h4 style="text-align: justify;">QUEM SOMOS</h4>
											<p style="text-align: justify;" id="quemSomosVirtude">[Breve introdução da Empresa]</p>
											<div class="clearboth"></div>
										</div>
									</div>
									
									<div style="" class="vc_col-sm-4 wpb_column column_container  _ height-full">
										<div id="text-block-5" class="mk-text-block  mk-animate-element right-to-left   ">
											<h2 style="text-align: center;">
												<span style="color: #ffa32b;"id="mensagemVirtude">&#8220;[Slogan da Empresa]&#8221;</span>
											</h2>
											<div class="clearboth"></div>
										</div>
									</div>
								</div>
								<div class="clearboth"></div>
							</div> 
							
							<div class="mk-skip-to-next" data-skin="light">
								<svg  class="mk-svg-icon" data-name="mk-jupiter-icon-arrow-bottom" data-cacheid="icon-58d12c9635618" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 121.6c0 8-3.2 16-8 22.4l-225.6 240c-6.4 6.4-14.4 9.6-24 9.6-8 0-16-3.2-22.4-9.6l-224-240c-11.2-12.8-11.2-33.6 1.6-44.8 12.8-12.8 32-11.2 44.8 1.6l201.6 214.4 203.2-216c11.2-12.8 32-12.8 44.8 0 4.8 6.4 8 14.4 8 22.4z"/></svg>
							</div> 
							<div class="clearboth"></div>
						</div>
					</div>
					
					<div class="mk-main-wrapper-holder">
						<div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
							<div class="theme-content no-padding">
								<div class="clearboth"></div>
							</div> 		
						</div> 			
					</div> 	
					<div class="mk-page-section-wrapper">
						<div id="page-section-7" class="mk-page-section self-hosted  full-width-7 js-el js-master-row     center-y"    data-intro-effect="false">
							<div class="mk-page-section-inner">
								<div class="mk-video-color-mask"></div>
								<div class="mk-section-preloader js-el" data-mk-component="Preloader">
									<div class="mk-section-preloader__icon"></div>
								</div>
								<div class="background-layer-holder">
									<div id="background-layer--7" class="background-layer  none-blend-effect js-el" data-mk-component="Parallax" data-parallax-config='{"speed" : 0.4 }'  data-mk-img-set='{"landscape":{"desktop":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/macbook-pro-and-coffe-cup-mockup-3.jpg","tablet":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/macbook-pro-and-coffe-cup-mockup-3-1024x768.jpg","mobile":"http:\/\/zoi.com.br\/virtude\/wp-content\/uploads\/2016\/09\/macbook-pro-and-coffe-cup-mockup-3-736x414.jpg"}}' >
									</div>
								</div>
							</div>
							
							<div class="page-section-content vc_row-fluid page-section-fullwidth">
								<div class="mk-padding-wrapper">
									<div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
										<div id="text-block-8" class="mk-text-block  mk-animate-element fade-in   ">
											<h3 style="text-align: center;"id="mensagemApresentacaoVirtude">[Proposta de trabalho para a Empresa]</h3>
											<div class="clearboth"></div>
										</div>
										
										<div class="wpb_row vc_inner vc_row    attched-false  mk-animate-element fade-in   vc_row-fluid ">
											<div class="mk-grid">
												<?php
												//$cont = 0;
												//while($cont<6){
												?>		
													<div id="divPortfolioVirtudeBase">		
														<div style="display: none;" class="wpb_column vc_column_container vc_col-sm-4">
															<div class="vc_column-inner ">			
																<div class="wpb_wrapper">
																	<div id="text-block-9" class="mk-text-block  mk-animate-element fade-in">
																		<h4 style="text-align: center;" id="tituloPortfolioVirtude[]">[Título do Produto ofertado pela Empresa]</h4>
																		<h5 style="text-align: justify;"id="descricaoPortfolioVirtude[]">[Descrição do Produto ofertado pela Empresa]</h5>
																		<div class="clearboth"></div>
																	</div>
																</div>
															</div>
														</div>		
													</div>	
											</div>
										</div>
											
										<div class="wpb_row vc_inner vc_row    attched-false  mk-animate-element fade-in   vc_row-fluid ">
											<div class="mk-grid">
												<div class="wpb_column vc_column_container vc_col-sm-4">
													<div class="vc_column-inner ">
														<div class="wpb_wrapper"></div>
													</div>
												</div>
												
												<div class="wpb_column vc_column_container vc_col-sm-4">
													<div class="vc_column-inner ">
														<div class="wpb_wrapper">
															<div id="mk-button-12" class="mk-button-container _ relative width-full   block text-center  ">
																<a  id="arqOrcamento" href="#"  onClick="ValidarOrcamento();" target="_blank" class="mk-button js-smooth-scroll mk-button--dimension-flat mk-button--size-large mk-button--corner-pointed text-color-light _ relative text-center font-weight-700 no-backface  letter-spacing-2 block">
																	<span class="mk-button--text">Visualizar orçamento</span>
																</a>
															</div>
														</div>
													</div>
												</div>
											
												<div class="wpb_column vc_column_container vc_col-sm-4">
													<div class="vc_column-inner ">
														<div class="wpb_wrapper"></div>
													</div>
												</div>		
											</div>		
										</div>
										<div class="clearboth"></div>
									</div>
								</div>
							</div>
          
<div class="mk-skip-to-next" data-skin="light">
	<svg  class="mk-svg-icon" data-name="mk-jupiter-icon-arrow-bottom" data-cacheid="icon-58dd9172893c8" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 121.6c0 8-3.2 16-8 22.4l-225.6 240c-6.4 6.4-14.4 9.6-24 9.6-8 0-16-3.2-22.4-9.6l-224-240c-11.2-12.8-11.2-33.6 1.6-44.8 12.8-12.8 32-11.2 44.8 1.6l201.6 214.4 203.2-216c11.2-12.8 32-12.8 44.8 0 4.8 6.4 8 14.4 8 22.4z"/></svg></div>
            
        
        <div class="clearboth"></div>
    </div>
</div>

<div class="mk-main-wrapper-holder">
	<div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
		<div class="theme-content no-padding">

			


<div class="clearboth"></div>
	</div> 		</div> 			</div> 

				
<div class="mk-page-section-wrapper">
    <div id="page-section-13" class="mk-page-section self-hosted  full-width-13 js-el js-master-row     center-y"    data-intro-effect="false">

        
            
            <div class="mk-page-section-inner">
                



<div class="mk-video-color-mask"></div>

                
                            </div>
            
            
        <div class="page-section-content vc_row-fluid mk-grid">
            <div class="mk-padding-wrapper">
<div style="" class="vc_col-sm-6 wpb_column column_container  _ height-full">
	
<div id="text-block-14" class="mk-text-block  mk-animate-element fade-in   ">

	
	<h4 style="text-align: left;">FALE COM A GENTE</h4>
<p style="text-align: left;">Aguardamos o seu contato<br />
para detalharmos a proposta,<br />
discutir sobre o projeto e<br />
comemoramos essa futura nova parceria!</p>
<div class="widgettitle" style="text-align: left;">
<p><span style="color: #ffcc00;">ONDE ESTAMOS</span></p>
</div>
<div class="textwidget">
<p style="text-align: left;" id="enderecoEmpresaContato">Rua Américo Luz, 521 / 905 – Bairro Gutierrez<br />
Belo Horizonte – MG – CEP: 30441-094<br />
Tel: 31 3371-2368</p>
<p style="text-align: left;">email: <a id="emailEmpresaContato" href="mailto:zoi@zoi.com.br" target="_blank">zoi@zoi.com.br</a></p>
</div>

	<div class="clearboth"></div>
</div>
</div>

<div style="" class="vc_col-sm-6 wpb_column column_container  _ height-full">
	
<div class="mk-contact-form-wrapper s_contact classic-style s_classic  ">
    <form id="mk-contact-form-15" class="mk-contact-form clearfix" method="post" novalidate="novalidate" enctype="multipart/form-data">

        <div class="mk-form-row ">
			<svg  class="mk-svg-icon" data-name="mk-li-user" data-cacheid="icon-58dd91728f4d4" style=" height:16px; width: 13px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 416 512"><path d="M270.382 265.777c33.169-24.98 55.301-69.146 55.301-119.615 0-78.012-52.695-141.257-117.714-141.257-65.003 0-117.714 63.245-117.714 141.257 0 50.48 22.133 94.65 55.332 119.63-82.139 26.371-141.609 103.341-141.625 194.236.016 25.995 21.09 47.067 47.086 47.067h313.904c25.995 0 47.071-21.072 47.086-47.067-.015-90.911-59.501-167.892-141.656-194.251zm15.389-166.892c-56.221-2.686-101.605-19.205-124.366-45.086 13.442-11.024 29.414-17.503 46.565-17.503 34.286 0 63.884 25.643 77.801 62.589zm-164.125 47.277c0-25.957 7.143-49.799 19.021-68.621 30.578 32.218 85.251 51.439 152.645 52.987.582 5.123.981 10.315.981 15.634 0 60.581-38.717 109.866-86.324 109.866-47.591.001-86.323-49.285-86.323-109.866zm243.306 329.542h-313.904c-8.645 0-15.68-7.039-15.695-15.669.015-95.182 77.464-172.616 172.647-172.616s172.632 77.434 172.647 172.59c0 8.656-7.035 15.695-15.695 15.695z"/></svg>		<input placeholder="Your Name" type="text" required="required" name="contact_name" class="text-input s_txt-input" value="" tabindex="90" />
</div>
<div class="mk-form-row ">
			<svg  class="mk-svg-icon" data-name="mk-li-call" data-cacheid="icon-58dd91728fa66" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M396.958 287.359c-42.196 0-78.79 23.803-97.19 58.704-60.198-25.474-108.38-73.771-133.739-134.022 34.985-18.378 58.88-55.01 58.88-97.267 0-60.682-49.186-109.866-109.866-109.866s-109.867 49.185-109.867 109.866c.031 216.268 175.046 391.629 391.185 392.287l.597.031.199-.015.399.015v-.031c60.405-.337 109.268-49.369 109.268-109.836 0-60.68-49.185-109.866-109.866-109.866zm.429 188.312h-.935c-198.412-.597-359.856-162.5-359.886-360.897 0-43.269 35.207-78.476 78.476-78.476s78.476 35.207 78.476 78.476c0 29.213-16.124 55.837-42.089 69.479-14.43 7.571-20.653 24.937-14.331 39.958 28.34 67.348 83.166 122.297 150.438 150.759 3.977 1.686 8.131 2.499 12.216 2.499 11.327 0 22.194-6.162 27.781-16.769 13.649-25.872 40.25-41.951 69.425-41.951 43.269 0 78.476 35.207 78.476 78.476 0 43.025-35.008 78.216-78.047 78.446zm-280.084-238.508c-1.763-3.97-6.384-5.763-10.353-4.001-3.962 1.747-5.755 6.376-4.009 10.346 11.665 26.378 26.876 51.162 45.223 73.663 1.556 1.901 3.809 2.897 6.093 2.897 1.74 0 3.494-.582 4.95-1.778 3.357-2.728 3.863-7.679 1.127-11.035-17.457-21.413-31.941-45.002-43.031-70.092z"/></svg>		
	<input placeholder="Your Phone Number" class="text-input s_txt-input" type="text" name="contact_phone" value="" tabindex="91" />
</div>
<div class="mk-form-row ">
			<svg  class="mk-svg-icon" data-name="mk-li-mail" data-cacheid="icon-58dd91728ff3f" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M460.038 75.505h-408.076c-26.003 0-47.086 21.083-47.086 47.086v266.818c0 26.002 21.082 47.086 47.086 47.086h408.075c26.002 0 47.086-21.083 47.086-47.086v-266.818c0-26.003-21.083-47.086-47.085-47.086zm11.908 324.001l-128.703-128.708 132.49-132.489v251.1c0 3.862-1.457 7.357-3.787 10.097zm-435.679-10.097v-251.039l131.493 131.497-128.581 128.581c-1.824-2.56-2.912-5.667-2.912-9.039zm153.688-97.352l39.138 39.138c7.173 7.181 16.722 11.135 26.876 11.135s19.703-3.954 26.876-11.135l38.203-38.204 112.104 112.112h-356.237l113.04-113.046zm270.083-185.161c.843 0 1.663.122 2.467.249l-201.854 201.857c-1.686 1.69-3.655 1.938-4.682 1.938-1.027 0-2.997-.249-4.683-1.938l-201.845-201.854c.827-.13 1.655-.253 2.522-.253h408.075z"/></svg>		
	<input placeholder="Your Email" class="text-input s_txt-input" type="email" required="required" name="contact_email" value="" tabindex="92" />
</div><div class="mk-form-row">
			<svg  class="mk-svg-icon" data-name="mk-li-pencil" data-cacheid="icon-58dd917290390" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M493.276 85.32l-66.59-66.598c-18.385-18.378-48.205-18.378-66.589 0l-306.662 306.654c-9.533 9.549-15.68 21.658-17.818 35.054l-30.257 127.293c-1.256 5.303.323 10.882 4.177 14.73 3.855 3.862 9.426 5.441 14.729 4.184l127.294-30.272c13.412-2.131 25.52-8.277 35.062-17.81l306.654-306.655c18.384-18.377 18.384-48.203 0-66.58zm-346.659 360.054l-1.165.184-1.15.261-102.509 24.385 24.639-103.659.184-1.18c.698-4.353 2.245-8.292 4.445-11.879l87.435 87.443c-3.572 2.192-7.511 3.755-11.879 4.445zm34.755-25.965l-88.784-88.791 234.171-234.156 22.195 22.194-234.126 234.126 11.097 11.096 234.125-234.124 22.194 22.194-234.124 234.125 11.096 11.096 234.133-234.125 22.194 22.194-234.171 234.171zm289.702-289.702l-33.337 33.337-88.783-88.776 33.337-33.337c6.116-6.131 16.078-6.131 22.194 0l66.59 66.582c6.122 6.116 6.122 16.079-.001 22.194z"/></svg>		
	<textarea required="required" placeholder="Your Message" class="mk-textarea s_txt-input" name="contact_content" tabindex="93"></textarea>
</div><div class="mk-form-row" style="float:left;">
    <button class="mk-progress-button mk-button contact-form-button mk-skin-button mk-button--dimension-flat text-color-light mk-button--size-medium" data-style="move-up" tabindex="94">
        <span class="mk-progress-button-content">
            ENVIAR        </span>
        <span class="mk-progress">
            <span class="mk-progress-inner"></span>
        </span>

        <span class="state-success">
            <svg  class="mk-svg-icon" data-name="mk-moon-checkmark" data-cacheid="icon-58dd917290e63" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M432 64l-240 240-112-112-80 80 192 192 320-320z"/></svg>        </span>

        <span class="state-error">
            <svg  class="mk-svg-icon" data-name="mk-moon-close" data-cacheid="icon-58dd91729145f" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M507.331 411.33l-.006-.005-155.322-155.325 155.322-155.325.006-.005c1.672-1.673 2.881-3.627 3.656-5.708 2.123-5.688.912-12.341-3.662-16.915l-73.373-73.373c-4.574-4.573-11.225-5.783-16.914-3.66-2.08.775-4.035 1.984-5.709 3.655l-.004.005-155.324 155.326-155.324-155.325-.005-.005c-1.673-1.671-3.627-2.88-5.707-3.655-5.69-2.124-12.341-.913-16.915 3.66l-73.374 73.374c-4.574 4.574-5.784 11.226-3.661 16.914.776 2.08 1.985 4.036 3.656 5.708l.005.005 155.325 155.324-155.325 155.326-.004.005c-1.671 1.673-2.88 3.627-3.657 5.707-2.124 5.688-.913 12.341 3.661 16.915l73.374 73.373c4.575 4.574 11.226 5.784 16.915 3.661 2.08-.776 4.035-1.985 5.708-3.656l.005-.005 155.324-155.325 155.324 155.325.006.004c1.674 1.672 3.627 2.881 5.707 3.657 5.689 2.123 12.342.913 16.914-3.661l73.373-73.374c4.574-4.574 5.785-11.227 3.662-16.915-.776-2.08-1.985-4.034-3.657-5.707z"/></svg>        </span>

    </button>
</div>
<input type="hidden" id="security" name="security" value="63cc2e4cb8" /><input type="hidden" name="_wp_http_referer" value="/virtude/voce/" /><input type="hidden" id="sh_id" name="sh_id" value="95"><input type="hidden" id="p_id" name="p_id" value="5">    <div class="contact-form-message clearfix"></div>   
    </form>
</div>

</div>
</div>
            <div class="clearboth"></div>
        </div>


            
            
<div class="mk-skip-to-next" data-skin="light">
	<svg  class="mk-svg-icon" data-name="mk-jupiter-icon-arrow-bottom" data-cacheid="icon-58dd9172967c4" style=" height:16px; width: 16px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M512 121.6c0 8-3.2 16-8 22.4l-225.6 240c-6.4 6.4-14.4 9.6-24 9.6-8 0-16-3.2-22.4-9.6l-224-240c-11.2-12.8-11.2-33.6 1.6-44.8 12.8-12.8 32-11.2 44.8 1.6l201.6 214.4 203.2-216c11.2-12.8 32-12.8 44.8 0 4.8 6.4 8 14.4 8 22.4z"/></svg></div>
            
        
        <div class="clearboth"></div>
    </div>
</div>

<div class="mk-main-wrapper-holder">
	<div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
		<div class="theme-content no-padding">

			

		<div class="clearboth"></div>
		                      
                      <div class="clearboth"></div>
                                            </div>
                                <div class="clearboth"></div>
                
                </div>
            </div>


                
        </div>          

<section id="mk-footer-unfold-spacer"></section>

<section id="mk-footer" class="" role="contentinfo" itemscope="itemscope" itemtype="https://schema.org/WPFooter" >
        <div class="footer-wrapper fullwidth-footer">
        <div class="mk-padding-wrapper">
            		<div class=""><section id="text-2" class="widget widget_text">			<div class="textwidget"><center>Todos os direitos reservados © 2017  .  Gostou do <b>Presentation? <a href="http://www.presentation.com.br" target="_blank">Clique aqui para solicitar o seu! </a></b></center></div>
		</section></div>
	            <div class="clearboth"></div>
        </div>
    </div>
        </section>
</div>
</div>

<div class="bottom-corner-btns js-bottom-corner-btns">

<a href="#top-of-page" class="mk-go-top  js-smooth-scroll js-bottom-corner-btn js-bottom-corner-btn--back">
	<svg  class="mk-svg-icon" data-name="mk-icon-chevron-up" data-cacheid="icon-58dd9172b2926" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792"><path d="M1683 1331l-166 165q-19 19-45 19t-45-19l-531-531-531 531q-19 19-45 19t-45-19l-166-165q-19-19-19-45.5t19-45.5l742-741q19-19 45-19t45 19l742 741q19 19 19 45.5t-19 45.5z"/></svg></a>

</div>



<div class="mk-fullscreen-search-overlay">
	<a href="#" class="mk-fullscreen-close"><svg  class="mk-svg-icon" data-name="mk-moon-close-2" data-cacheid="icon-58dd9172ba7a0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M390.628 345.372l-45.256 45.256-89.372-89.373-89.373 89.372-45.255-45.255 89.373-89.372-89.372-89.373 45.254-45.254 89.373 89.372 89.372-89.373 45.256 45.255-89.373 89.373 89.373 89.372z"/></svg></a>
	<div class="mk-fullscreen-search-wrapper">
		<p>Start typing and press Enter to search</p>
		<form method="get" id="mk-fullscreen-searchform" action="http://zoi.com.br/virtude">
			<input type="text" value="" name="s" id="mk-fullscreen-search-input" />
			<i class="fullscreen-search-icon"><svg  class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-58dd9172babfb" style=" height:25px; width: 23.214285714286px; "  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1664 1792"><path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg></i>
		</form>
	</div>
</div>	
<div style="text-align: center;" id="fixed">
<div class="alert alert-success" id="msgSucesso" hidden>
  <h3><strong>Sucesso!</strong></h3>
	<p id="textoSucesso"></p>
</div>
<div class="alert alert-warning" id="msgAviso" hidden>
  <h3><strong>Aviso!</strong></h3>
	<p id="textoAviso"></p>
</div>
<div class="alert alert-danger" id="msgErro" hidden>
  <h4><strong>Erro!</strong></h4>
	<p id="textoErro"></p>
</div>
<div class="alert" id="loading" hidden>
	<button class="btn btn-lg btn-default"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> processando...</button>
</div>
</div>

<div id="uploadModal" class="modal fade">
  <div class="modal-dialog">
	  <div class="modal-content">
		  <div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			  <h4 class="modal-title">Adicionar Imagem do Produto</h4>
		  </div>
			<form name="formUploadProduto" id="formUploadProduto" method="post" onsubmit="CheckfilesImage(this);">
			  <div class="form-group labelPreto col-xs-12">
				<div class="col-xs-10">
					<input type="file" class="form-control" id="arquivo" name="arquivo" accept="image/png, image/jpeg, image/jpg" aria-describedby="fileHelp">
				</div>
				<input class="form-control" name="idPortModal" id="idPortModal" value="" style="display: none;"></input>
				<div class="col-xs-2">
					<input class="btn btn-default" type="button" id="btnEnviarProduto" value="Enviar" /><br /><br />
				</div>

			  </div>
				
			</form>
		  <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
		  </div>
	  </div>
  </div>
</div>

<div class="navBar" <?php if ($_SESSION['nivel_session'] == 3) { echo "style=display:none;"; } ?> id="detalhePonto" onmouseout="TornarTransparente();" onmouseover="RemoverTransparencia();">
	<div class="relativeWrapper">
		<span class="callMenu">Editar
		</span>
	<div id="exTab1" class="container">	
	<ul  class="nav nav-pills">
		<?php
		if ($_SESSION['nivel_session'] == 1){
		?>
		<li><a href="#3a" data-toggle="tab">Empresa</a>
		</li>
		<?php
		}
		if ($_SESSION['nivel_session'] <= 2){
		?>
		<li><a href="#4a" data-toggle="tab">Cliente</a>
		</li>
		<?php
		}
		?>
		<li><a href="#1a" data-toggle="tab">Layout</a>
		</li>
		<li><a href="#2a" data-toggle="tab">Portfolio</a>
		</li>
		<?php
		if ($_SESSION['nivel_session'] == 1){
		?>
		<li><a href="#5a" data-toggle="tab">Admin</a>
		</li>
		<?php
		}
		?>
		<li><a href="php/logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
	</ul>

		<div class="tab-content clearfix">
		  <div class="tab-pane" id="1a">
					<form method="post" id="DadosLayout" action="" enctype="multipart/form-data" method="post" onSubmit="SalvarLayout();return false;">
						<?php
						if ($_SESSION['nivel_session']!=4){
						?>
						<div class="form-group">
						  <select class="form-control" name="empresaLayout" id="empresaLayout" onchange="BuscarClientesEmpresa(this, clienteLayout)">
						  </select>
						</div>
						<div class="form-group">
						  <select class="form-control" name="clienteLayout" id="clienteLayout" onchange="BuscarLayout(empresaLayout, this); MostraValor(this, virtude, 1, 1);">
						  </select>
						</div>
						<?php
						}
						?>
						<div style="display: none;">
						  <input class="form-control" type="text" placeholder="id" name="idLayout" maxlength="100" id="idLayout" value=""></input>
						</div>
						<div class="form-group">
						  <input class="form-control" type="text" placeholder="Saudação" name="saudacao" maxlength="100" id="saudacao" value="" oninput="MostraValor(saudacao, saudacaoVirtude,false,true)"required></input>
						</div>
						<div class="form-group">
						  <input class="form-control" type="text" placeholder="Introdução" name="introducao" maxlength="100" id="introducao" value="" oninput="MostraValor(introducao, introducaoVirtude)"required></input>
						</div>
						<div class="form-group">
						  <textarea class="form-control" type="text" placeholder="Quem Somos" rows="5" name="quemSomos" maxlength="2000" id="quemSomos" value="" oninput="MostraValor(quemSomos, quemSomosVirtude)"required></textarea>
						</div>
						<div class="form-group">
						  <input class="form-control" type="text" name="mensagem" placeholder="Mensagem" id="mensagem" value="" oninput="MostraValor(mensagem, mensagemVirtude)"required></input>
						</div>
						<div class="form-group">
						  <input class="form-control" type="text" name="mensagemApresentacao" placeholder="Mensagem de apresentação do portfolio" id="mensagemApresentacao" value="" oninput="MostraValor(mensagemApresentacao, mensagemApresentacaoVirtude)"required></input>
						</div>
						<?php
						if ($_SESSION['nivel_session'] <= 2){
						?>
						<div class="checkbox">
						  <label class="labelPreto"><input type="checkbox" value="">Tornar este layout padrão</label>
						</div>
						
						<input type="submit" name="submit" class="btn btn-success btn-lg btn-block" value="Salvar">

						<?php
						}
						?>
					</form>
			</div>
			<div class="tab-pane" id="2a">
					<form id="DadosPortfolio" method="post" action="" enctype="multipart/form-data" onSubmit="SalvarPortfolio();return false;">
						<?php
						if ($_SESSION['nivel_session']!=4){
						?>
						<div class="form-group">
						  <select class="form-control" name="empresaPortfolio" id="empresaPortfolio" onchange="BuscarClientesEmpresa(this, clientePortfolio)">
						  </select>
						</div>
						<div class="form-group">
						  <select class="form-control" id="clientePortfolio" name="clientePortfolio" onChange="Mudarestado(clientePortfolio,'formPortfolio'); BuscarPortfolio(empresaPortfolio, this);">
						  </select>

						</div>
						<?php
						}
						?>						
						  <input class="" type="text" name="idMaxPortfolio" id="idMaxPortfolio" value="" style="margin-bottom: 5px;" hidden />
						  <input class="" type="text" name="idLayoutPortfolio" id="idLayoutPortfolio" value="" style="margin-bottom: 5px;" hidden />
						  <input class="" type="text" name="contPortfolio" id="contPortfolio" value="" style="margin-bottom: 5px;" hidden />
						<div id="formPortfolio" hidden>
							<hr style="height:2px; border:none; color:#000; background-color:silver; margin-top: 0px; margin-bottom: 5px;"/>
							<div style="" id="divPortfolioList">
							</div>
							<input class="btn btn-primary btn-lg btn-sm" type="button" id="adicionarPortfolio" value="Adicionar Portfolio" style="margin-bottom: 5px;" />
							<?php
							if ($_SESSION['nivel_session'] <= 2){
							?>
							<input type="submit" name="submit" class="btn btn-success btn-lg btn-block" value="Salvar">
							<?php
							}
							?>
						</div>
					</form>
			</div>
			<div class="tab-pane" id="3a">

					<form name="formEmpresaGeral" id="formEmpresaGeral" action="" method="post">
						<div class="form-group">
							<div class="col-xs-9">
								<select class="form-control" id="empresa2" name="empresa2" onChange="Mudarestado(empresa2,'formEmpresa'); Mudarestado(empresa2,'formUpload'); Mudarestado(empresa2,'formSalvarEmpresa')">
								</select>
							</div>
							<div class="col-xs-3">
								<button type="button" class="btn btn-default" onClick="LimparCamposEmpresa()">Novo</button>
							</div>
						</div><br></br>
						<div id="formEmpresa" hidden>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="text" placeholder="Seu Nome Fantasia" name="nomeFantasiaEmpresa" maxlength="100" id="nomeFantasiaEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="text" placeholder="Sua Razão Social" name="razaoSocialEmpresa" maxlength="100" id="razaoSocialEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="text" placeholder="Seu Contato" name="contatoEmpresa" maxlength="100" id="contatoEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="email" placeholder="Seu E-Mail" name="emailEmpresa" maxlength="100" id="emailEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-12">
									<input class="form-control" type="text" placeholder="Seu Endereço" name="enderecoEmpresa" maxlength="150" id="enderecoEmpresa" required></input>
								</div>
							</div>						
							<div class="form-group">
								<div class="col-xs-6">
									<input class="form-control" type="text" placeholder="Sua Cidade" name="cidadeEmpresa" maxlength="100" id="cidadeEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-3">
									<input class="form-control" type="text" placeholder="Seu UF" name="estadoEmpresa" maxlength="2" id="estadoEmpresa" required></input>
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-3">
									<input class="form-control"  placeholder="Seu CEP" data-mask="99999-999" name="cepEmpresa" maxlength="10" id="cepEmpresa" required></input>
								</div>
							</div>
							<div class="form-group" style="display: none;">
								<div class="col-xs-12">
									<input class="form-control" type="text" placeholder="" name="imgLogo" maxlength="2" id="imgLogo" value="" hidden></input>						
								</div>
							</div>
						</div>
					</form>
					<form name="formUpload" id="formUpload" method="post" onsubmit="CheckfilesImage(this);"hidden>
					  <div class="form-group labelPreto col-xs-12">
						<div class="col-xs-1">
							<label>Logo:</label>
						</div>
						<div class="col-xs-9">
							<input type="file" class="form-control" id="arquivo" name="arquivo" accept="image/png, image/jpeg" aria-describedby="fileHelp">
						</div>
						<div class="col-xs-2">
							<input class="btn btn-default" type="button" id="btnEnviar" value="Enviar" /><br /><br />
						</div>

					  </div>
						
					</form>
					<div id="formSalvarEmpresa" hidden>
						<input type="button" name="btnSalvarEmpresa" class="btn btn-success btn-lg btn-block" value="Salvar" onclick="SalvarEmpresa();">
						<input type="button" name="enviarEmailEmpresa" id = "enviarEmailEmpresa" class="btn btn-primary btn-lg btn-block" value="Enviar Email">
					</div>
			</div>
			<div class="tab-pane" id="4a">
					<form enctype="multipart/form-data" id="DadosCliente" action="#" method="post" onSubmit="SalvarCliente();return false;">
						<div class="col-xs-9">
						<div class="form-group">
						  <select class="form-control" name="empresaCliente" id="empresaCliente" onChange="BuscarClientesEmpresa(this, cliente2);">
						  </select>
						</div>
						</div>
						<div class="form-group">
						<div class="col-xs-9">
						  <select class="form-control" id="cliente2" name="cliente2" onChange="Mudarestado(cliente2,'formCliente'); Mudarestado(cliente2,'formUploadOrcamento'); Mudarestado(cliente2,'formSalvarCliente')">
						  </select>
						 </div>
						 <div class="col-xs-3">
						 <button type="button" class="btn btn-default" onClick="LimparCamposCliente()">Novo</button>
						 </div>
						</div><br></br><br></br>
						<div id="formCliente" hidden>
						<div class="form-group">
						  <input class="form-control" type="text" placeholder="Nome do seu cliente" name="nomeCliente" maxlength="100" id="nomeCliente" value="" required></input>
						</div>
						<div class="form-group">
						  <input class="form-control" type="text" placeholder="Contato do seu cliente" name="contatoCliente" maxlength="100" id="contatoCliente" value="" required></input>
						</div>
						<div class="form-group">
						  <input class="form-control" type="email" placeholder="E-Mail do seu cliente" name="emailCliente" maxlength="100" id="emailCliente" value="" required></input>
						  <input type="text" class="form-control" id="pathArquivo" name="pathArquivo" value="" aria-describedby="fileHelp" hidden>
						</div>
						
						</div>
					</form>
					<form name="formUploadOrcamento" id="formUploadOrcamento" onSubmit="CheckfilesPDF(arquivo);" method="post" hidden>
					  <div class="form-group labelPreto">
						<div class="col-xs-2">
							<label>Orçamento:</label>
						</div>
						<div class="col-xs-8">
							<input type="file" class="form-control" id="arquivo" name="arquivo" aria-describedby="fileHelp">
						</div>
						<div class="col-xs-2">
							<input class="btn btn-default" type="button" id="btnEnviarOrcamento" value="Enviar" /><br /><br />
						</div>
					  </span>
					  </div>
					</form>
					<div id="formSalvarCliente" hidden>
						<input type="button" name="btnSalvarCliente" class="btn btn-success btn-lg btn-block" value="Salvar" onclick="SalvarCliente();">
						<input type="button" name="enviarEmailCliente" id = "enviarEmailCliente" class="btn btn-primary btn-lg btn-block" value="Enviar Email">
					</div>
			</div>

			<div class="tab-pane" id="5a">

					<form name="formAdmin" id="formAdmin" action="" method="post" >

						<div class="form-group" style="display: none;">
							<div class="col-xs-12">
								<input class="form-control" type="text" placeholder="" name="imgFundo" maxlength="2" id="imgFundo" value="" hidden></input>						
							</div>
						</div>
						<div class="form-group" style="display: none;">
							<div class="col-xs-12">
								<input class="form-control" type="text" placeholder="" name="imgFundoPortfolio" maxlength="2" id="imgFundoPortfolio" value="" hidden></input>						
							</div>
						</div>
						
					</form>
					<form name="formUploadFundo" id="formUploadFundo" method="post">
					  <div class="form-group labelPreto col-xs-12">
						<div class="col-xs-12">
							<label>Fundo:</label>
						</div>
						<div class="col-xs-10">
							<input type="file" class="form-control" id="arquivo" name="arquivo" aria-describedby="fileHelp">
						</div>
						<div class="col-xs-2">
							<input class="btn btn-default" type="button" id="btnEnviarFundo" value="Enviar" /><br /><br />
						</div>

					  </div>
					</form>
					<form name="formUploadFundoPortfolio" id="formUploadFundoPortfolio" method="post">
					  <div class="form-group labelPreto col-xs-12">
						<div class="col-xs-12">
							<label>Fundo Portfolio:</label>
						</div>
						<div class="col-xs-10">
							<input type="file" class="form-control" id="arquivo" name="arquivo" aria-describedby="fileHelp">
						</div>
						<div class="col-xs-2">
							<input class="btn btn-default" type="button" id="btnEnviarFundoPortfolio" value="Enviar" /><br /><br />
						</div>

					  </div>					  
						
					</form>
					<div id="formSalvarEmpresa" hidden>
						<input type="button" name="btnSalvarAdmin" class="btn btn-success btn-lg btn-block" value="Salvar" onclick="SalvarAdmin();">
					</div>
			</div>
			
		</div>

	</div>
</div>
</div>



<footer id="mk_page_footer">
    <script type="text/javascript">
    php = {
        hasAdminbar: false,
        json: (null != null) ? null : "",
        jsPath: 'http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/js'
      };
    </script><script type='text/javascript' src='http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/js/plugins/wp-enqueue/smoothscroll.js?ver=5.2'></script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-includes/js/comment-reply.min.js?ver=4.7.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_login_object = {"ajaxurl":"http:\/\/zoi.com.br\/virtude\/wp-admin\/admin-ajax.php","redirecturl":"http:\/\/zoi.com.br\/virtude\/voce\/","loadingmessage":"Sending user info, please wait..."};
/* ]]> */
</script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-content/themes/jupiter/assets/js/core-scripts.js?ver=5.2'></script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-includes/js/wp-embed.min.js?ver=4.7.2'></script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-content/plugins/js_composer_theme/assets/js/dist/js_composer_front.min.js?ver=4.12.1'></script>
<script type='text/javascript' src='http://zoi.com.br/virtude/wp-content/uploads/mk_assets/components-production.min.js?ver=1482439390'></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="js/jasny-bootstrap.min.js"></script>
		<script type="text/javascript">
				</script>
	<script type="text/javascript">	window.get = {};	window.get.captcha = function(enteredCaptcha) {
                  return jQuery.get(ajaxurl, { action : "mk_validate_captcha_input", captcha: enteredCaptcha });
              	};</script>    <script>
        // Run this very early after DOM is ready
        (function ($) {
            // Prevent browser native behaviour of jumping to anchor
            // while preserving support for current links (shared across net or internally on page)
            var loc = window.location,
                hash = loc.hash;

            // Detect hashlink and change it's name with !loading appendix
            if(hash.length && hash.substring(1).length) {
                var $topLevelSections = $('.vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments');
                var $section = $topLevelSections.filter( '#' + hash.substring(1) );
                // We smooth scroll only to page section and rows where we define our anchors.
                // This should prevent conflict with third party plugins relying on hash
                if( ! $section.length )  return;
                // Mutate hash for some good reason - crazy jumps of browser. We want really smooth scroll on load
                // Discard loading state if it already exists in url (multiple refresh)
                hash = hash.replace( '!loading', '' );
                var newUrl = hash + '!loading';
                loc.hash = newUrl;
            }
        }(jQuery));
    </script>
    <script>
$('.callMenu').click(function() {
    $('.navBar').toggleClass('slideMenu');
});
</script>
<script src="js/editor_layout.js"></script>
<script src="upload.js"></script>  
</footer>  
</body>
</html>