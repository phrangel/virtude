<?php
/* Importa o arquivo onde a função de upload está implementada */
require_once('funcao_upload.php');

/* Captura o arquivo selecionado */
$arquivo = $_FILES['arquivo'];


$arquivo_tmp = $_FILES[ 'arquivo' ][ 'tmp_name' ];
$nome = $_FILES[ 'arquivo' ][ 'name' ];


// Pega a extensão
$extensao = pathinfo ( $nome, PATHINFO_EXTENSION );

// Converte a extensão para minúsculo
$extensao = strtolower ( $extensao );

$novoNome = uniqid ( time () ) .'.'.$extensao;

/*Define os tipos de arquivos válidos (No nosso caso, só imagens)*/

$tipos = array('pdf','jpg', 'png', 'gif', 'psd', 'bmp');


/* Chama a função para enviar o arquivo */
$enviar = uploadFile($arquivo, 'uploads/', $tipos, $novoNome);

$data['sucesso'] = true;

$data['msg'] = $enviar['caminho'];

/* Codifica a variável array $data para o formato JSON */
echo json_encode($data);