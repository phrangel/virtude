<?php

session_start();

if(isset($_SESSION['user_session'])!="")
{
	header("Location: index.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen"> 
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="js/validation.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" src="js/script.js"></script>
<link rel="shortcut icon" href="http://zoi.com.br/virtude/wp-content/uploads/2017/03/favicon.png"  />

</head>

<body>
    
<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin" method="post" id="login-form">
		<img src="img/Logo_Presentation.png" class="img-responsive center-block" alt="Imagem Responsiva">
        <h3 class="form-signin-heading">Área Restrita</h3><hr />
        
        <div id="error">
        <!-- error will be shown here ! -->
        </div>
        
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Email do Usuário" name="usuario" id="usuario" />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Senha" name="senha" id="senha" />
        </div>
       
     	<hr />
        
        <div class="form-group">
            <button type="submit" class="btn btn-default" name="btn-login" id="btn-login">
    		<span class="glyphicon glyphicon-log-in"></span> &nbsp; Acesso Credenciado
			</button> 
        </div>  

        <div class="form-group">
            <button type="submit" class="btn btn-default" name="btn-visitante" id="btn-visitante">
    		<span class="glyphicon glyphicon-log-in"></span> &nbsp; Acesso Visitante
			</button> 
        </div> 
		
      </form>

    </div>
    
</div>
    
<script src="js/bootstrap.min.js"></script>

</body>
</html>