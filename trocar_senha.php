
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Trocar Senha</title>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen"> 
<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" src="js/script.js"></script>
<link rel="shortcut icon" href="http://zoi.com.br/virtude/wp-content/uploads/2017/03/favicon.png"  />

</head>

<body>
 
<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin" method="post" id="trocar-form"  data-toggle="validator" role="form">
		<img src="img/Logo_Presentation.png" class="img-responsive center-block" alt="Imagem Responsiva">
        <h3 class="form-signin-heading">Área Restrita</h3><hr />
        
        <div id="error">
        <!-- error will be shown here ! -->
        </div>
        
        <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" name="usuario" id="usuario" data-error="Por favor, informe seu e-mail." required/>
		<div class="help-block with-errors"></div>
        <span id="check-e"></span>
        </div>
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Senha Atual" name="senhaAtual" id="senhaAtual" data-error="Por favor, informe a senha atual." required/>
		<div class="help-block with-errors"></div>
        </div>  
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Nova Senha" name="novaSenha" id="novaSenha" data-error="Por favor, informe a nova senha." required/>
		<div class="help-block with-errors"></div>
        </div>        
        <div class="form-group">
        <input type="password" class="form-control" placeholder="Confirmar a Senha" name="confSenha" id="confSenha" 
		 data-match="#novaSenha" data-match-error="Atenção! As senhas não estão iguais."/>
		 <div class="help-block with-errors"></div>		
        </div>
        <div id="error">
        <!-- error will be shown here ! -->
        </div>       
     	<hr />
        
        <div class="form-group">
            <button type="submit" class="btn btn-default" name="btn-trocar" id="btn-trocar">
    		<span class="glyphicon glyphicon-log-in"></span> &nbsp; Trocar
			</button> 
        </div>  
		
		
      </form>

    </div>
    
</div>
<script src="js/validator.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>