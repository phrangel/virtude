

$('document').ready(function()
{ 

	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}

	var usuario = getUrlVars()["usuario"];
	var senha = getUrlVars()["senha"];

	if ((typeof usuario !== "undefined")  && (typeof senha !== "undefined")){

		$.ajax({
			
		type : 'POST',
		url  : 'php/login_process.php',
		data : 'btn-login=ok&usuario=' + usuario + '&senha=' + senha,
		beforeSend: function()
		{	
			$("#error").fadeOut();
			$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; processando ...');
		},
		success :  function(response)
		   {		
				if(response=="ok"){
					
					$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Acessando ...');
					setTimeout(' window.location.href = "index.php"; ',4000);
				}
				else{
								
					$("#error").fadeIn(1000, function(){						
						$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
										$("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Acesso Credenciado');
								});
				}
		  }
		});
		return false;
	}	
	
		
	 $("#btn-login").click(function(e){
		 $("#login-form").validate({
		  rules:
		  {
				senha: {
				required: true,
				},
				usuario: {
				required: true,
				email: true
				},
		   },
		   messages:
		   {
				senha:{
						  required: "Por favor, informe sua senha"
						 },
				usuario: "Por favor, informe seu email",
		   },
		   submitHandler: submitForm	
		   });  

		   function submitForm()
		   {		
				var data = $("#login-form").serialize();
					
				$.ajax({
					
				type : 'POST',
				url  : 'php/login_process.php',
				data : data,
				beforeSend: function()
				{	
					$("#error").fadeOut();
					$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; processando ...');
				},
				success :  function(response)
				   {		
						if(response=="ok"){
							
							$("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Acessando ...');
							setTimeout(' window.location.href = "index.php"; ',4000);
						}
						else{
										
							$("#error").fadeIn(1000, function(){						
								$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
												$("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Acesso Credenciado');
										});
						}
				  }
				});
				return false;
			}

	});
	$('#btn-visitante').click(function(e){
		
		var data = $("#login-form").serialize();
			
		$.ajax({
			
		type : 'POST',
		url  : 'php/login_process.php',
		data : data,
		beforeSend: function()
		{	
			$("#error").fadeOut();
			$("#btn-visitante").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; processando ...');
		},
		success :  function(response)
		   {		
				if(response=="ok"){
					
					$("#btn-visitante").html('<img src="btn-ajax-loader.gif" /> &nbsp; Acessando ...');
					setTimeout(' window.location.href = "index.php"; ',4000);
				}
				else{
								
					$("#error").fadeIn(1000, function(){						
						$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
										$("#btn-visitante").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Acesso Visitante');
								});
				}
		  }
		});
		
		return false;
	});

	 $("#btn-trocar").click(function(e){
		
				var data = $("#trocar-form").serialize();
					
				$.ajax({
					
				type : 'POST',
				url  : 'php/validar_senha_atual.php',
				data : data,
				beforeSend: function()
				{	
					$("#error").fadeOut();
					$("#btn-trocar").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; processando ...');
				},
				success :  function(response)
				   {		
						if(response=="ok"){

							$.ajax({
								
							type : 'POST',
							url  : 'php/alterar_senha_atual.php',
							data : data,
							beforeSend: function()
							{	
								$("#error").fadeOut();
								$("#btn-trocar").html('<img src="btn-ajax-loader.gif" /> &nbsp; Alterando...');
							},
							success :  function(response)
							   {		
									if(response=="ok"){
										
										setTimeout(' window.location.href = "index.php"; ',4000);
									}
									else{
													
										$("#error").fadeIn(1000, function(){						
											$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
															$("#btn-trocar").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Trocar');
													});
									}
							  }
							});						
							
						}
						else{
										
							$("#error").fadeIn(1000, function(){						
								$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
												$("#btn-trocar").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Trocar');
										});
						}
				  }
				});
				return false;
	})

});