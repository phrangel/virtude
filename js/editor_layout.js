var campos_max          = 3;   //max de 10 campos
var qtdePortfolio = document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1; // campos iniciais
var x = document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1;

		
$(document).ready(function() {
	
		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_nivel_acesso.php", 
			  success: function(dados){

				var login = JSON.parse(dados);
				
				if(login.nivel == 1){

					InicializarAdmin();
				}

				if(login.nivel == 2){

					InicializarEmpresa(login.idEmpresa, login.nomeFantasia);
				}
				if(login.nivel == 3){

					InicializarCliente(login.id);
				}
				
			}
		});	
		
		var InicializarEmpresa = function(idEmpresa, nomeFantasia) { 
			$.ajax({
				  type: "POST", //METODO
				  url: "php/buscar_clientes_empresa.php", //Pagina a ser chamada
				  data: "empresa2=" + idEmpresa, //Os dados a serem enviados
				  success: function(dados){ //Funcao de callback

					var clientes = JSON.parse(dados);
					
					var option = document.createElement("option");
					
					option = '<option value="'+idEmpresa+'">'+nomeFantasia+'</option>';

					var optionCliente = document.createElement("option");
					
					optionCliente = '<option value="">Selecione o cliente</option>'

					if (clientes != null){
						$.each(clientes, function(i, cliente){
						  optionCliente += '<option value="'+cliente.id+'">'+cliente.nome+'</option>';
						})
					}	
					
					$("#cliente2").html(optionCliente).show();
					$("#clienteLayout").html(optionCliente).show();
					$("#clientePortfolio").html(optionCliente).show();
					$("#empresaPortfolio").html(option).show();
					$("#empresaCliente").html(option).show();
					$("#empresaLayout").html(option).show();
					
				}
			});
		};
		var InicializarCliente = function(idLogin) { 

			$.ajax({
				  type: "POST", //METODO
				  url: "php/buscar_layout_cliente.php", //Pagina a ser chamada
				  data: "idLogin=" + idLogin, //Os dados a serem enviados
				  success: function(dados){ //Funcao de callback

					var clientes = JSON.parse(dados);

					document.getElementById('virtude').innerHTML ='<strong>' + clientes.nome + '</strong>';
					document.getElementById('mensagemVirtude').innerHTML =clientes.mensagem;
					document.getElementById('mensagemApresentacaoVirtude').innerHTML =clientes.apresentacao;
					document.getElementById('quemSomosVirtude').innerHTML=clientes.quemSomos;
					document.getElementById('introducaoVirtude').innerHTML=clientes.introducao;
					document.getElementById('saudacaoVirtude').innerHTML='<strong>' + clientes.saudacao + '</strong>';					

				}
			});
		};
		
		var InicializarAdmin = function() { 

			$.ajax({
					type: "POST", //METODO
					url: "php/buscar_empresas.php", //Pagina a ser chamada
					success: function(dados){ //Funcao de callback


					var empresas = JSON.parse(dados);



					var option = document.createElement("option");

					option = '<option value="">Selecione a empresa</option>'

					if (empresas != null){
						$.each(empresas, function(i, empresa){
						  option += '<option value="'+empresa.id+'">'+empresa.nomeFantasia+'</option>';
						})
					}
					
					$("#empresa2").html(option).show();
					$("#empresaCliente").html(option).show();
					$("#empresaLayout").html(option).show();
					$("#empresaPortfolio").html(option).show();

				}
			});
		};
		
       $('#arqOrcamento').on('click', function() {
           var orcamento = $(this);
		   if (orcamento.attr('href') == '#') {
			document.getElementById('textoAviso').innerHTML='Não há nenhum orçamento definido para o cliente!';
						$('#msgAviso').fadeIn( "slow", function() {
								setTimeout(function () {
									$('#msgAviso').fadeOut("slow"); 
								}, 2000);
							});
		   }
	   });
		   
        //var campos_max          = 3;   //max de 10 campos
        //var qtdePortfolio = 1;//document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1; // campos iniciais
		//var x = 1;//document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1;
		
        $('#adicionarPortfolio').click (function(e) {

				//qtdePortfolio = document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1; // campos iniciais
				//x = document.getElementById("contPortfolio").value != ''?document.getElementById("contPortfolio").value: 1;
				//alert(qtdePortfolio);
				e.preventDefault();     //prevenir novos clicks
                if (qtdePortfolio <= campos_max) {
						
                        $('#divPortfolioList').append('<div id="divPortfolioNovo'+ x +'">' + 
                                '<input class="form-control" type="text" placeholder="Titulo Portfolio" name="tituloPortfolio' + x + '" id="tituloPortfolio'+ x +'" oninput="MostraValor(tituloPortfolio'+ x +', tituloPortfolioVirtude'+ x +');" required>' + 
								'<input class="form-control" type="text" placeholder="Titulo Portfolio" name="imagemPortfolio' + x + '" id="imagemPortfolio'+ x +'" style="display: none;">' + 
								'<textarea class="form-control" type="text" placeholder="Descrição Portfolio" rows="3" maxlength="2000" id="descricaoPortfolio' + x + '" name="descricaoPortfolio'+ x +'" oninput="MostraValor(descricaoPortfolio'+ x +', descricaoPortfolioVirtude'+ x +');" required/>' + 
                                '<input class="form-control" name="idPortfolio' + x + '" id="idPortfolio'+ x +'" value=' + x +' type="hidden">' +
								'<input class="btn btn-danger btn-lg btn-sm" type="button" value="Remover" id="remover_campo" style="margin-bottom: 5px;">' + 
								'<input class="btn btn-primary btn-lg btn-sm" type="button" value="Adicionar Imagem" id="adicionar_produto" style="margin-bottom: 5px; margin-left: 5px">' +
								'<br/><hr style="height:2px; border:none; color:#000; background-color:silver; margin-top: 0px; margin-bottom: 5px;"/>' + 							
                                '</div>');

								
						$('#divPortfolioVirtudeBase').append('<div id="divPortfolioVirtudeNovo' + x + '" style="display: block;">' + 
								'<div class="wpb_column vc_column_container vc_col-sm-4">' +
								'<div class="wpb_single_image wpb_content_element vc_align_center">' +
								'<figure class="wpb_wrapper vc_figure">' +
								'<div class="vc_single_image-wrapper   vc_box_border_grey">' + 
								'<img id="img-produto' + x + '" width="300" height="150" src="http://zoi.com.br/virtude/wp-content/uploads/2016/09/port-02.jpg" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 300px) 100vw, 300px" />' +
								'</div></figure></div><div class="vc_column-inner "><div class="wpb_wrapper">' + 
								'<h4 style="text-align: center;" id="tituloPortfolioVirtude' + x + '">' + "[Título Portfolio]" + '</h4>' + 
								'<h5 style="text-align: justify;" id="descricaoPortfolioVirtude' + x + '">[Descrição do Portfolio]</h5>' +  
								'</div></div></div></div>');
					qtdePortfolio++;
					document.getElementById("idMaxPortfolio").value = x;
					x++;
						
                }
        });
		
        // Remover o div anterior
        $('#divPortfolioList').on("click","#remover_campo",function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
				var campo = 'idPortfolio' + (x - 1);
				var id = $(this).parent('div').find('input[name*=idPortfolio]').val(); 
				$('div').remove('#divPortfolioVirtudeNovo' + id);
                qtdePortfolio--;
				//document.getElementById("contPortfolio").value;
        });
		
        $('#divPortfolioList').on("click","#adicionar_produto",function(e) {
                e.preventDefault();
				$("#uploadModal").modal({backdrop: 'static', keyboard: false});
				$("#idPortModal").val($(this).parent('div').find('input[name*=idPortfolio]').val());
        });
		
		$('#enviarEmailEmpresa').click (function(e) {
			
			$('#loading').fadeIn( "slow");
			
			if ($('#emailEmpresa').val() != '')
			{
				$.ajax({
					type: "POST",
					url: "php/buscar_login.php",
					data: "email=" + document.getElementById("emailEmpresa").value, 
					success: function (dados)
					{

						var login = JSON.parse(dados);
						
						if (login != null)
						{
						
							var corpoEmail = '<b>Bem Vindo ao Presentation!</b><br>' +
											 'Seu usuario de acesso: ' + login.usuario + '<br>' +
											 'Sua senha de acesso: ' + login.senha + '<br>' +
											 'Para acessar seu template para personalizacao, acesse: http://localhost:8080/virtude/login.php <br>' + 
											 'Caso queira trocar a senha, acesse: http://localhost:8080/virtude/trocar_senha.php';
							$.ajax({
								type: "POST",
								url: "php/enviar_email.php",
								data: "corpoEmail=" + corpoEmail + "&email=" + login.usuario + "&senha=" + login.senha + "&nivel=2",
								success: function (dados)
								{
									
									$('#loading').fadeOut( "slow", function() {
									
										document.getElementById('textoSucesso').innerHTML='Email enviado para ' + login.usuario;
										$('#msgSucesso').fadeIn( "slow", function() {
											setTimeout(function () {
												$('#msgSucesso').fadeOut("slow"); 
											}, 3000);
										});
									});
								},
								error: function(XMLHttpRequest, textStatus, errorThrown) 
								{ 
									$('#loading').fadeOut( "slow", function() {
									
										document.getElementById('textoErro').innerHTML='Falha no envio do email. Erro ao recuparar os dados da empresa para o envio do email. Detalhe do Erro: ' + textStatus + '. Erro: ' + errorThrown;
													$('#msgErro').fadeIn( "slow", function() {
															setTimeout(function () {
																$('#msgErro').fadeOut("slow"); 
															}, 3500);
														});
									});
								}
							});	
							
						}
						else{
							$('#loading').fadeOut( "slow", function() {
							
								document.getElementById('textoErro').innerHTML='Falha no envio do email. Nenhuma empresa encontrada para o email informado!';
											$('#msgErro').fadeIn( "slow", function() {
													setTimeout(function () {
														$('#msgErro').fadeOut("slow"); 
													}, 3500);
												});
							});
						}

					}
				});			
			}
			else
			{
				
				$('#loading').fadeOut( "slow", function() {
				
					document.getElementById('textoErro').innerHTML='Nenhum email informado! Favor selecione uma empresa e tente novamente!';
					$('#msgErro').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgErro').fadeOut("slow"); 
							}, 3500);
					});
				});
			}
		});
		
		$('#enviarEmailCliente').click (function(e) {
			
			$('#loading').fadeIn( "slow");

			if ($('#emailCliente').val() != '')
			{
				
				$.ajax({
					type: "POST",
					url: "php/buscar_login.php",
					data: "email=" + document.getElementById("emailCliente").value, 
					success: function (dados)
					{

						var login = JSON.parse(dados);
						
						if (login != null){
						
							var corpoEmail = "<p><b>Oi, " + document.getElementById("nomeCliente").value + "!</b></p> " + 
									"<p>Somos da empresa " + $('#empresaCliente option:selected').text() + "</p>";
			
							$.ajax({
								type: "POST",
								url: "php/enviar_email.php",
								data: "corpoEmail=" + corpoEmail + "&email=" + login.usuario + "&senha=" + login.senha + "&nivel=3",
								success: function (dados)
								{
									$('#loading').fadeOut( "slow", function() {
									
										document.getElementById('textoSucesso').innerHTML='Email enviado para ' + login.usuario;
										$('#msgSucesso').fadeIn( "slow", function() {
											setTimeout(function () {
												$('#msgSucesso').fadeOut("slow"); 
											}, 3000);
										});
									});
								}
							});
						}
						else{
							$('#loading').fadeOut( "slow", function() {
								document.getElementById('textoErro').innerHTML='Falha no envio do email. Nenhum cliente encontrado para o email informado!';
									$('#msgErro').fadeIn( "slow", function() {
											setTimeout(function () {
												$('#msgErro').fadeOut("slow"); 
											}, 3500);
								});
							});
						}

					},
					error: function(XMLHttpRequest, textStatus, errorThrown) 
					{ 
						$('#loading').fadeOut( "slow", function() {
							document.getElementById('textoErro').innerHTML='Falha no envio do email. Erro ao recuparar os dados do cliente para o envio do email. Detalhe do Erro: ' + textStatus + '. Erro: ' + errorThrown;
										$('#msgErro').fadeIn( "slow", function() {
												setTimeout(function () {
													$('#msgErro').fadeOut("slow"); 
												}, 3500);
							});
						});
					}
				});	
			}
			else
			{
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoErro').innerHTML='Nenhum email informado! Favor selecione um cliente e tente novamente!';
					$('#msgErro').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgErro').fadeOut("slow"); 
							}, 3500);
					});
				});
			}

		});
		
    $('#btnEnviar').click(function(){
	
        $('#formUpload').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {

            },        
            success: function(data) {
                $('progress').attr('value','100');
                $('#porcentagem').html('100%');                
                if(data.sucesso == true){
                    
					var img = document.getElementById('img-upload');
					img.src = data.msg;

					document.getElementById('imgLogo').value = data.msg;

                }
                else{
                    $('#resposta').html(data.msg);
                }                
            },
            error : function(){

                $('#resposta').html('Erro ao enviar requisição!!!');
				
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: false
        }).submit();
		return false;
    });
	
    $('#btnEnviarProduto').click(function(){
	
        $('#formUploadProduto').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {

            },        
            success: function(data) {
                $('progress').attr('value','100');
                $('#porcentagem').html('100%');                
                if(data.sucesso == true){
					
					var campo = "img-produto" + document.getElementById("idPortModal").value;
					var campoTela = "imagemPortfolio" + document.getElementById("idPortModal").value;
					var img = document.getElementById(campo);
					document.getElementById(campoTela).value = data.msg;

					img.src = data.msg;

					//document.getElementById('imgLogo').value = data.msg;

                }
                else{
                    $('#resposta').html(data.msg);
                }                
            },
            error : function(){

                $('#resposta').html('Erro ao enviar requisição!!!');
				
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: false
        }).submit();
		
		$('#uploadModal').modal('toggle');
		
		return false;
		
    });

    $('#btnEnviarFundo').click(function(){

        $('#formUploadFundo').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {

            },        
            success: function(data) {
                $('progress').attr('value','100');
                $('#porcentagem').html('100%');                
                if(data.sucesso == true){
                    
					var img = document.getElementById('img-fundo');
					img.src = data.msg;
					document.getElementById('imgFundo').value = data.msg;

                }
                else{
                    $('#resposta').html(data.msg);
                }                
            },
            error : function(){

                $('#resposta').html('Erro ao enviar requisição!!!');
				
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: false
        }).submit();
		return false;
    });
	
    $('#btnEnviarFundoPortfolio').click(function(){

        $('#formUploadFundoPortfolio').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {
            },        
            success: function(data) {
                $('progress').attr('value','100');
                $('#porcentagem').html('100%');                
                if(data.sucesso == true){
                    
					var img = document.getElementById('img-fundo-port');
					img.src = data.msg;
					document.getElementById('imgFundoPortfolio').value = data.msg;

                }
                else{
                    $('#resposta').html(data.msg);
                }                
            },
            error : function(){

                $('#resposta').html('Erro ao enviar requisição!!!');
				
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: false
        }).submit();
		return false;
    });
	
	$('#btnEnviarOrcamento').click(function(){

        $('#formUploadOrcamento').ajaxForm({     
			uploadProgress: function(event, position, total, percentComplete) {
            },     
            success: function(data) {
                if(data.sucesso == true){
					
					var orcamento = document.getElementById('arqOrcamento');
					orcamento.href =  data.msg;
					document.getElementById('pathArquivo').value = data.msg;

                }
                else{
                    $('#resposta').html(data.msg);
                }                
            },
            error : function(){

                $('#resposta').html('Erro ao enviar requisição!!!');
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: false
        }).submit();
		
		return false;
    });
})

function CheckfilesImage(){
    var fup = document.getElementById('arquivo');
    var fileName = fup.value;
    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if(ext =="jpeg" || ext=="png" || ext=="jpg"){
        return true;
    }
    else{
		document.getElementById('textoAviso').innerHTML='Extensão inválida! As extensões válidas são JPG e PNG!';
					$('#msgAviso').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgAviso').fadeOut("slow"); 
							}, 2000);
						});
        return false;
    }
}

function CheckfilesPDF(arquivo){
    //var fup = document.getElementById('arquivo');
    var fileName = arquivo.value;

    var ext = fileName.substring(fileName.lastIndexOf('.') + 1);

    if(ext == "pdf"){
        return true;
    }
    else{
		document.getElementById('textoAviso').innerHTML='Extensão inválida! É aceito apenas arquivos com a extensão PDF!';
					$('#msgAviso').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgAviso').fadeOut("slow"); 
							}, 3000);
						});
        return false;
    }
}

function Teste(campo, campo2){
	
	document.getElementById(campo2.id).innerHTML = document.getElementById(campo.id).value;
}

function TornarTransparente(){
	document.getElementById("detalhePonto").style.opacity = 0.2;
	
}

function RemoverTransparencia(){
	document.getElementById("detalhePonto").style.opacity = 1;
	
}

var BuscarClientesEmpresa = function (campoEmpresa, campoCliente){

		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_clientes_empresa.php", //Pagina a ser chamada
			  data: "empresa2=" + campoEmpresa.value, //Os dados a serem enviados
			  success: function(dados){ //Funcao de callback

				var clientes = JSON.parse(dados);
				var option = document.createElement("option");
				var campo= '#' + campoCliente.id;
				
				option = '<option value="">Selecione o cliente</option>'

				if (clientes != null){
					$.each(clientes, function(i, cliente){
					  option += '<option value="'+cliente.id+'">'+cliente.nome+'</option>';
					})
				}	
				
				$(campo).html(option).show();
				
			}
		});
}

function BuscarLayout(campoEmpresa, campoCliente){

		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_layout.php", //Pagina a ser chamada
			  data: "empresaId=" + campoEmpresa.value + "&clienteId=" + campoCliente.value, //Os dados a serem enviados
			  success: function(dados){ //Funcao de callback
			  
				var layout = JSON.parse(dados);
				
				if (layout != null){
					document.getElementById('idLayout').value=layout.id;
					document.getElementById('idLayoutPortfolio').value=layout.id;
					document.getElementById('mensagem').value=layout.mensagem;
					document.getElementById('mensagemApresentacao').value=layout.apresentacao;
					document.getElementById('quemSomos').value=layout.quemSomos;
					document.getElementById('introducao').value=layout.introducao;
					document.getElementById('saudacao').value=layout.saudacao;
					document.getElementById('virtude').innerHTML ='<strong>' + layout.nomeCliente + '</strong>';
					document.getElementById('mensagemVirtude').innerHTML =layout.mensagem;
					document.getElementById('mensagemApresentacaoVirtude').innerHTML =layout.apresentacao;
					document.getElementById('quemSomosVirtude').innerHTML=layout.quemSomos;
					document.getElementById('introducaoVirtude').innerHTML=layout.introducao;
					document.getElementById('saudacaoVirtude').innerHTML='<strong>' + layout.saudacao + '</strong>';
				}
				else{
					document.getElementById('idLayout').value='';
					document.getElementById('mensagem').value='';
					document.getElementById('mensagemApresentacao').value='';
					document.getElementById('quemSomos').value='';
					document.getElementById('introducao').value='';
					document.getElementById('saudacao').value='';
					document.getElementById('idLayoutPortfolio').value='';
					document.getElementById('mensagemVirtude').innerHTML = '';
					document.getElementById('mensagemApresentacaoVirtude').innerHTML = '';
					document.getElementById('quemSomosVirtude').innerHTML= '';
					document.getElementById('introducaoVirtude').innerHTML= '';
					document.getElementById('saudacaoVirtude').innerHTML= '';

				}
			  }		
		});
}

function BuscarPortfolio(campoEmpresa, campoCliente){
		
		for (idt = 1; idt <= document.getElementById('idMaxPortfolio').value; idt++) {
			$('div').remove('#divPortfolioVirtudeNovo' + idt);
			$('div').remove('#divPortfolioNovo' + idt);
		}

		document.getElementById('idMaxPortfolio').value = 0;
		x=1;
		qtdePortfolio = 1;		
		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_portfolio.php", //Pagina a ser chamada
			  data: "empresaId=" + campoEmpresa.value + "&clienteId=" + campoCliente.value, //Os dados a serem enviados
			  success: function(dados){ //Funcao de callback
			  
				var portfolios = JSON.parse(dados);

				if (portfolios != null){

					var iGeral = 1;
					$.each(portfolios, function(i, portfolio){
						document.getElementById('idLayoutPortfolio').value=portfolio.idLayout;		
						$('#divPortfolioList').append('<div id="divPortfolioNovo'+(i+1)+'">' + 
								'<input class="form-control" type="text" placeholder="Titulo Portfolio" name="tituloPortfolio' + (i+1) + '" id="tituloPortfolio'+ (i+1) +'" oninput="MostraValor(tituloPortfolio'+ (i+1) +', tituloPortfolioVirtude'+ (i+1) +');" value="' + portfolio.titulo +'" required>' + 
								'<input class="form-control" type="text" placeholder="Titulo Portfolio" name="imagemPortfolio' + (i+1) + '" id="imagemPortfolio'+ (i+1) +'" value="' + portfolio.imgProduto +'" style="display: none;">' + 
								'<textarea class="form-control" type="text" placeholder="Descrição Portfolio" rows="3" maxlength="2000" id="descricaoPortfolio' + (i+1) + '" name="descricaoPortfolio'+ (i+1) +'" oninput="MostraValor(descricaoPortfolio'+ (i+1) +', descricaoPortfolioVirtude'+ (i+1) +');"  required>' + portfolio.descricao + '</textarea>' + 
								'<input class="form-control" name="idPortfolio' + (i+1) + '" id="idPortfolio'+ (i+1) +'" value=' + (i+1) +' type="hidden">' +
								'<input class="btn btn-danger btn-lg btn-sm" type="button" value="Remover" id="remover_campo" style="margin-bottom: 5px;">' + 
								'<input class="btn btn-primary btn-lg btn-sm" type="button" value="Adicionar Imagem" id="adicionar_produto" style="margin-bottom: 5px; margin-left: 5px">' +
								'<br/><hr style="height:2px; border:none; color:#000; background-color:silver; margin-top: 0px; margin-bottom: 5px;"/>' + 							
								'</div>');
					   
						$('#divPortfolioVirtudeBase').append('<div id="divPortfolioVirtudeNovo' + (i+1) + '" style="display: block;">' + 
								'<div class="wpb_column vc_column_container vc_col-sm-4">' +
								'<div class="wpb_single_image wpb_content_element vc_align_center">' +
								'<figure class="wpb_wrapper vc_figure">' +
								'<div class="vc_single_image-wrapper   vc_box_border_grey">' + 
								'<img id="img-produto' + (i+1) + '" width="300" height="150" src="' + portfolio.imgProduto + '" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 300px) 100vw, 300px" />' +
								'</div></figure></div><div class="vc_column-inner "><div class="wpb_wrapper">' + 
								'<h4 style="text-align: center;" id="tituloPortfolioVirtude' + (i+1) + '">' + portfolio.titulo + '</h4>' + 
								'<h5 style="text-align: justify;" id="descricaoPortfolioVirtude' + (i+1) + '">' + portfolio.descricao + '</h5>' +  
								'</div></div></div></div>');
													
						
						document.getElementById("idMaxPortfolio").value = (i + 1);
						document.getElementById("contPortfolio").value = (i + 1);
						iGeral = iGeral + 1;
						x++;
						qtdePortfolio++;
						
					})
					
					document.getElementById("idMaxPortfolio").value = iGeral;
					document.getElementById("contPortfolio").value = iGeral;
				}
			  },
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					document.getElementById('textoErro').innerHTML='Erro buscar portfolio. Status: ' + textStatus + '. Erro: ' + errorThrown;
								$('#msgErro').fadeIn( "slow", function() {
										setTimeout(function () {
											$('#msgErro').fadeOut("slow"); 
										}, 3500);
									});
						}			  
		});
}

function SalvarPortfolio()
{
	 var dados = $('#DadosPortfolio').serialize();

	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'php/salvar_portfolio.php',
		async: true,
		data: dados,
		success: function(id) {

			document.getElementById('textoSucesso').innerHTML='As informações de portfolio foram salvas!';
			$('#msgSucesso').fadeIn( "slow", function() {
					setTimeout(function () {
						$('#msgSucesso').fadeOut("slow"); 
					}, 1500);
				});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			document.getElementById('textoErro').innerHTML='Erro ao salvar as informações do portfolio. Status: ' + textStatus + '. Erro: ' + errorThrown;
						$('#msgErro').fadeIn( "slow", function() {
								setTimeout(function () {
									$('#msgErro').fadeOut("slow"); 
								}, 3500);
							});
                }
	});

	return false;
}

function SalvarCliente()
{
	
	$('#loading').fadeIn();
	
	var dados = $('#DadosCliente').serialize();

	var novo = true;
	
	if ((document.getElementById('empresaCliente').value)>0 && (document.getElementById('cliente2').value)>0){
		novo = false;
	}
	
	if (novo){
	 
		$.ajax({
			type: 'POST',
			url: 'php/validar_clientes_empresa.php',
			data: "nome=" + document.getElementById('nomeCliente').value + '&empresaId=' + document.getElementById('empresaCliente').value,
			success: function(dado) {
				
				var clienteExiste = JSON.parse(dado);
				
				if (clienteExiste != null){
					$('#loading').fadeOut( "slow", function() {
						document.getElementById('textoAviso').innerHTML='Já existe um cliente cadastrado com este nome!';
						$('#msgAviso').fadeIn( "slow", function() {
								setTimeout(function () {
									$('#msgAviso').fadeOut("slow"); 
								}, 2500);
						});
					});
				}
				else{

					$.ajax({
						type: 'POST',
						url: 'php/salvar_login_cliente.php',
						async: true,
						data: dados,
						success: function(id) {

							if (id != 0){

								$.ajax({
									type: 'POST',
									url: 'php/salvar_cliente.php',
									async: true,
									data: dados + "&idLogin=" + id,
									success: function(id) {

										if (id != 0){
											var selectCliente = document.getElementById("cliente2");
											var option = document.createElement("option");
											option.text = document.getElementById('nomeCliente').value;
											option.value = id;
											selectCliente.add(option);
										}
										$('#loading').fadeOut( "slow", function() {
											document.getElementById('textoSucesso').innerHTML='As informações de cliente foram salvas!';
											$('#msgSucesso').fadeIn( "slow", function() {
													setTimeout(function () {
														$('#msgSucesso').fadeOut("slow"); 
													}, 1500);
											});
										});
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) { 
										$('#loading').fadeOut( "slow", function() {
											document.getElementById('textoErro').innerHTML='Erro ao salvar as informações do cliente. Status: ' + textStatus + '. Erro: ' + errorThrown;
														$('#msgErro').fadeIn( "slow", function() {
																setTimeout(function () {
																	$('#msgErro').fadeOut("slow"); 
																}, 3500);
											});
										});
									}
								});	
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							$('#loading').fadeOut( "slow", function() {
								document.getElementById('textoErro').innerHTML='Erro ao salvar as informações do login do cliente. Status: ' + textStatus + '. Erro: ' + errorThrown;
								$('#msgErro').fadeIn( "slow", function() {
										setTimeout(function () {
											$('#msgErro').fadeOut("slow"); 
										}, 3500);
								});
							});
						}				
					});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoErro').innerHTML='Erro ao validar as informações do cliente. Status: ' + textStatus + '. Erro: ' + errorThrown;
					$('#msgErro').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgErro').fadeOut("slow"); 
							}, 3500);
					});
				});
							
			}
		});
	}
	else {
		$.ajax({
			type: 'POST',
			url: 'php/salvar_cliente.php',
			async: true,
			data: dados,
			success: function(id) {
				
				if (id != 0){
					var selectCliente = document.getElementById("cliente2");
					var option = document.createElement("option");
					option.text = document.getElementById('nomeCliente').value;
					option.value = id;
					selectCliente.add(option);
				}
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoSucesso').innerHTML='As informações de cliente foram salvas!';
					$('#msgSucesso').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgSucesso').fadeOut("slow"); 
							}, 1500);
					});
				});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoErro').innerHTML='Erro ao salvar as informações do cliente. Status: ' + textStatus + '. Erro: ' + errorThrown;
					$('#msgErro').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgErro').fadeOut("slow"); 
							}, 3500);
					});
				});
			}
		});
	}

	return false;
}

function SalvarLayout()
{
	$('#loading').fadeIn();
	
	var dados = $('#DadosLayout').serialize();

	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'php/salvar_layout.php',
		async: true,
		data: dados,
		success: function(id) {
			$('#loading').fadeOut( "slow", function() {
				document.getElementById('textoSucesso').innerHTML = 'As informações de layout foram salvas!';
				$('#msgSucesso').fadeIn( "slow", function() {
						setTimeout(function () {
							$('#msgSucesso').fadeOut("slow"); 
						}, 1500);
				});
			});
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			$('#loading').fadeOut( "slow", function() {
				document.getElementById('textoErro').innerHTML='Erro ao salvar as informações do layout. Status: ' + textStatus + '. Erro: ' + errorThrown;
				$('#msgErro').fadeIn( "slow", function() {
						setTimeout(function () {
							$('#msgErro').fadeOut("slow"); 
						}, 3500);
				});
			});
        }
	});

	return false;
}
function SubmeterEmpresa()
{
	var form = document.getElementById("DadosEmpresa");
	form.submit();
	event.preventDefault();
	return false;
}

function SalvarEmpresa()
{
	
	$( "#formEmpresaGeral").submit();
}

$( "#formEmpresaGeral" ).submit(function( event ) {
	
	$('#loading').fadeIn();

	var dados = $('#formEmpresaGeral').serialize();
	
	var novo = true;
	
	if ((document.getElementById('empresa2').value)>0){
		novo = false;
	}
	
	if (novo){
		$.ajax({
			type: 'POST',
			url: 'php/validar_empresa.php',
			data: "nome=" + document.getElementById('nomeFantasiaEmpresa').value,
			success: function(dado) {

				var empresaExiste = JSON.parse(dado);
				
				if (empresaExiste != null){
					$('#loading').fadeOut( "slow", function() {
						document.getElementById('textoAviso').innerHTML='Já existe uma empresa cadastrada com este nome!';
						$('#msgAviso').fadeIn( "slow", function() {
								setTimeout(function () {
									$('#msgAviso').fadeOut("slow"); 
								}, 2500);
						});
					});
				}
				else{
					$.ajax({
						type: 'POST',
						url: 'php/salvar_empresa.php',
						async: true,
						data: dados,
						success: function(id) {
							if (id != 0){
								var selectEmpresa = document.getElementById("empresa2");
								var option = document.createElement("option");
								option.text = document.getElementById('nomeFantasiaEmpresa').value;
								option.value = id;
								selectEmpresa.add(option);	
							}
							$('#loading').fadeOut( "slow", function() {
								document.getElementById('textoSucesso').innerHTML='As informações de empresa foram salvas!';
								$('#msgSucesso').fadeIn( "slow", function() {
										setTimeout(function () {
											$('#msgSucesso').fadeOut("slow"); 
										}, 1500);
								});
							});
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) { 
							$('#loading').fadeOut( "slow", function() {
								document.getElementById('textoErro').innerHTML='Erro ao salvar as informações da empresa. Status: ' + textStatus + '. Erro: ' + errorThrown;
									$('#msgErro').fadeIn( "slow", function() {
											setTimeout(function () {
												$('#msgErro').fadeOut("slow"); 
											}, 3500);
									});			
							});
						}
					});
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoErro').innerHTML='Erro ao validar as informações da empresa. Status: ' + textStatus + '. Erro: ' + errorThrown;
					$('#msgErro').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgErro').fadeOut("slow"); 
							}, 3500);
					});
				});
							
			}
		});
	}
	else{
		$.ajax({
			type: 'POST',
			url: 'php/salvar_empresa.php',
			async: true,
			data: dados,
			success: function(id) {
				if (id != 0){
					var selectEmpresa = document.getElementById("empresa2");
					var option = document.createElement("option");
					option.text = document.getElementById('nomeFantasiaEmpresa').value;
					option.value = id;
					selectEmpresa.add(option);	
				}
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoSucesso').innerHTML='As informações de empresa foram salvas!';
					$('#msgSucesso').fadeIn( "slow", function() {
							setTimeout(function () {
								$('#msgSucesso').fadeOut("slow"); 
							}, 1500);
					});
				});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				$('#loading').fadeOut( "slow", function() {
					document.getElementById('textoErro').innerHTML='Erro ao salvar as informações da empresa. Status: ' + textStatus + '. Erro: ' + errorThrown;
								$('#msgErro').fadeIn( "slow", function() {
										setTimeout(function () {
											$('#msgErro').fadeOut("slow"); 
										}, 3500);
					});
				});
			}
		});		
	}
	return false;
})
	   
function MostraValor(campoOrigem, campoDestino, campoText = false, campoStrong = false) {
	 
    var valorCampo = campoText == true ? document.getElementById(campoOrigem.id).options[document.getElementById(campoOrigem.id).selectedIndex].text : document.getElementById(campoOrigem.id).value;
	document.getElementById(campoDestino.id).innerHTML = campoStrong == true ?'<strong>' + valorCampo + '</strong>' : valorCampo; 
}
function LimparCamposCliente(){
	document.getElementById('cliente2').value='';
	document.getElementById('nomeCliente').value='';
	document.getElementById('contatoCliente').value='';
	document.getElementById('emailCliente').value='';
	document.getElementById('pathArquivo').value='';
	document.getElementById('formCliente').style.display = 'block';
	document.getElementById('formSalvarCliente').style.display = 'block';
	document.getElementById('formUploadOrcamento').style.display = 'block';
	var orcamento = document.getElementById('arqOrcamento');
	orcamento.href =  '';	
}
function LimparCamposEmpresa(){
	document.getElementById('empresa2').value='';
	document.getElementById('nomeFantasiaEmpresa').value='';
	document.getElementById('razaoSocialEmpresa').value='';
	document.getElementById('contatoEmpresa').value='';
	document.getElementById('emailEmpresa').value='';
	document.getElementById('enderecoEmpresa').value='';
	document.getElementById('cidadeEmpresa').value='';
	document.getElementById('estadoEmpresa').value='';
	document.getElementById('cepEmpresa').value='';
	document.getElementById('imgLogo').value='';
	document.getElementById('formEmpresa').style.display = 'block';
	document.getElementById('formSalvarEmpresa').style.display = 'block';
	document.getElementById('formUpload').style.display = 'block';	
	var img = document.getElementById('img-upload');
	img.src = 'uploads/LogoAqui.png';

}
function Mudarestado(campo, el) {
		
	if(campo.value != "")
		document.getElementById(el).style.display = 'block';
	else
		document.getElementById(el).style.display = 'none';
	
	if (campo.id == 'cliente2') {
		var orcamento = document.getElementById('arqOrcamento');
		orcamento.href =  '';
		document.getElementById('pathArquivo').value = '';		
		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_cliente.php", //Pagina a ser chamada
			  data: "cliente2=" + document.getElementById('cliente2').value, //Os dados a serem enviados
			  success: function(dados){ //Funcao de callback
				var cliente = JSON.parse(dados);
				document.getElementById('cliente2').value=cliente.id;
				document.getElementById('empresaCliente').value=cliente.idEmpresa;
				document.getElementById('nomeCliente').value=cliente.nome;
				document.getElementById('contatoCliente').value=cliente.contato;
				document.getElementById('emailCliente').value=cliente.email;
				document.getElementById('pathArquivo').value=cliente.docOrcamento;
				var orcamento = document.getElementById('arqOrcamento');
				orcamento.href =  (cliente.docOrcamento != null?cliente.docOrcamento:'#');
				document.getElementById('pathArquivo').value = cliente.docOrcamento;
				}
		});
	}
	else {
		document.getElementById('imgLogo').value='';
		var img = document.getElementById('img-upload');
		img.src = '';		
		$.ajax({
			  type: "POST", //METODO
			  url: "php/buscar_empresa.php", //Pagina a ser chamada
			  data: "empresa2=" + document.getElementById('empresa2').value, //Os dados a serem enviados
			  success: function(dados){ //Funcao de callback
				var empresa = JSON.parse(dados);
				document.getElementById('empresa2').value=empresa.id;
				document.getElementById('nomeFantasiaEmpresa').value=empresa.nomeFantasia;
				document.getElementById('razaoSocialEmpresa').value=empresa.razaoSocial;
				document.getElementById('contatoEmpresa').value=empresa.contato;
				document.getElementById('emailEmpresa').value=empresa.email;
				document.getElementById('enderecoEmpresa').value=empresa.endereco;
				document.getElementById('cidadeEmpresa').value=empresa.cidade;
				document.getElementById('estadoEmpresa').value=empresa.estado;
				document.getElementById('cepEmpresa').value=empresa.cep;
				document.getElementById('imgLogo').value=empresa.imgLogo;
				var img = document.getElementById('img-upload');
				img.src = empresa.imgLogo;
				}
			});
	}
 }
			