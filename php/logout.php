<?php
	session_start();
	unset($_SESSION['user_session']);
	unset($_SESSION['nivel_session']);
	
	if(session_destroy())
	{
		header("Location: ../login.php");
	}
?>