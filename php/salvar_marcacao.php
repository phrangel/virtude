<?php
error_reporting (E_ALL & ~ E_NOTICE & ~ E_DEPRECATED);
session_start();
$categoria = $_POST['categoria'];
$nomeCategoria = $_POST['nome'];
$descricao = $_POST['descricao'];
$latlong = $_POST['latlong'];
$map = $_POST['map'];
$regrex = '/\((.*?)\,/';

// Usa o REGEX:
preg_match_all($regrex, $latlong, $resultado);

foreach($resultado[1] as $texto){
  $lat = $texto;
};

$regrex = '/\,(.*?)\)/';

// Usa o REGEX:
preg_match_all($regrex, $latlong, $resultado);

foreach($resultado[1] as $texto){
  $long = $texto;
};

/******
 * Upload de imagens
 ******/

// verifica se foi enviado um arquivo

if ( isset( $_FILES['img']['name'] ) && $_FILES['img']['error'] == 0 ) {

    $arquivo_tmp = $_FILES[ 'img' ][ 'tmp_name' ];
    $nome = $_FILES[ 'img' ][ 'name' ];

    // Pega a extensão
    $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );

    // Converte a extensão para minúsculo
    $extensao = strtolower ( $extensao );

    // Somente imagens, .jpg;.jpeg;.gif;.png
    // Aqui eu enfileiro as extensões permitidas e separo por ';'
    // Isso serve apenas para eu poder pesquisar dentro desta String
    if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
        // Cria um nome único para esta imagem
        // Evita que duplique as imagens no servidor.
        // Evita nomes com acentos, espaços e caracteres não alfanuméricos
        $novoNome = uniqid ( time () ) .'.'.$extensao;

        // Concatena a pasta com o nome
        $destino = '../img/'. $novoNome;

        // tenta mover o arquivo para o destino
        if ( @move_uploaded_file ( $arquivo_tmp, $destino ) ) {
            $_SESSION['uploadErro'] = "";
        }
        else
            $_SESSION['uploadErro'] = 'Erro ao salvar o arquivo. Aparentemente você não tem permissão de escrita.<br />';
    }
    else
        $_SESSION['uploadErro'] = 'Você poderá enviar apenas arquivos "*.jpg;*.jpeg;*.gif;*.png"<br />';
}
else
    $_SESSION['uploadErro'] = 'Você não enviou nenhum arquivo!';

$imagem = $novoNome;

include_once("conexao.php");

$categoria = ($categoria == 'Patrimônio Histórico' ? 'Patrimonio' : $categoria);

$result = mysql_query("insert into marcador (categoria, nome, descricao,img,latitude,longitude) values ('$categoria', '$nomeCategoria','$descricao','$imagem','$lat','$long')");

if (!$result) {
    $_SESSION['salvarErro'] = "Erro ao salvar os dados ($sql): " . mysql_error();
    exit;
}
header("location: ../index.php");

?>
