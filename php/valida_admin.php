<?php
error_reporting (E_ALL & ~ E_NOTICE & ~ E_DEPRECATED);
session_start();

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

header("Content-type: text/xml");

// Start XML file, echo parent node
echo '<users>';

// Iterate through the rows, printing XML nodes for each
// Add to XML document node
echo '<user ';
echo 'admin="' . $_SESSION['loginAdmin'] . '" ';
echo '/>';
// End XML file
echo '</users>';

?>
