<?php

/******
 * Upload de imagens
 ******/

error_reporting (E_ALL & ~ E_NOTICE & ~ E_DEPRECATED);

 // verifica se foi enviado um arquivo
 
if ( isset( $_FILES['img']['name'] ) && $_FILES['img']['error'] == 0 ) {

    $arquivo_tmp = $_FILES[ 'img' ][ 'tmp_name' ];
    $nome = $_FILES[ 'img' ][ 'name' ];

    // Pega a extens�o
    $extensao = pathinfo ( $nome, PATHINFO_EXTENSION );

    // Converte a extens�o para min�sculo
    $extensao = strtolower ( $extensao );

    // Somente imagens, .jpg;.jpeg;.gif;.png
    // Aqui eu enfileiro as extens�es permitidas e separo por ';'
    // Isso serve apenas para eu poder pesquisar dentro desta String
    if ( strstr ( '.jpg;.jpeg;.gif;.png', $extensao ) ) {
        // Cria um nome �nico para esta imagem
        // Evita que duplique as imagens no servidor.
        // Evita nomes com acentos, espa�os e caracteres n�o alfanum�ricos
        $novoNome = uniqid ( time () ) .'.'.$extensao;

        // Concatena a pasta com o nome
        $destino = '../img/'. $novoNome;

        // tenta mover o arquivo para o destino
        if ( @move_uploaded_file ( $arquivo_tmp, $destino ) ) {
            $_SESSION['uploadErro'] = "";
			echo $destino;			
        }
        else
            echo 'Erro ao salvar o arquivo. Aparentemente voc� n�o tem permiss�o de escrita.<br />';
    }
    else
        echo 'Voc� poder� enviar apenas arquivos "*.jpg;*.jpeg;*.gif;*.png"<br />';
}
else
    echo 'Voc� n�o enviou nenhum arquivo!';

?>